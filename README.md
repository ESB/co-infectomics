# About

Analysis of CO-INFECTOMICS data: metagenomic (metaG/MG) and metatranscriptomic (metaT/MT) sequencing data from stool samples of SARS-CoV-2 negative (N=60) and SARS-CoV-2 positive (N=65) study participants (adults cohort).

# Setup

## Cloning the repository

```bash
git clone --recurse-submodules <repo https/ssh URL>
```
If you cloned the project but forgot `--recurse-submodules` do
```bash
git submodule update --init --recursive
```
in the cloned repository.

## Conda

See [here](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html) for the `conda` user guide.

```bash
# install miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod u+x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh # follow the instructions
```

Create the main `snakemake` environment:
```bash
conda env create -f requirements.yaml
```

## Configuration

Main config file for **all** workflows is `config/config.yaml`.

All workflows have a `snakemake` profile (`workflow_<name>/profiles/iris_slurm/`) including a `snakemake` config file (`config.yaml`) and a `slurm` config file (`slurm.yaml`) for execution on the HPC cluster.

See the `README.md` inside each workflow folder for more information on the executed steps and configuration.

**IMPORTANT**
> Execute the workflows `workflow_preproc/` and `workflow_assembly/` first using **ONE** sample only!!!
> 
> This has to be done as the included tools `imp3` and `pathofact` are `snakemake` workflows and need to create their environments.
> The easiest way to do this is to run one sample only so all `conda` environments can be created before starting any other samples.

**RECOMMENDED**
> For computationally intensive workflows (`workflow_preproc/` and `workflow_assembly/`), it is recommended to run the analysis on sample subsets/batches instead of the complete dataset.
> See `scripts/split_samples.sh` below.

# Repository structure

## Workflows

- `workflow_qc_raw/`: QC of raw data
  - **not** part of the main data analysis
  - does **not** require `config/config.yaml` or other config files or inputs
  - **there should be no need to execute this workflow**
- `workflow_preproc/`: preprocessing metaG/metaT FASTQ files
  - to be executed first
- `workflow_postproc/`: postprocessing of the metaG/metaT FASTQ files
  - to be executed after the preprocessing workflow
- `workflow_profiling/`: taxonomic and functional profiling of metaG/metaT data
  - to be executed after the preprocessing workflow
- `workflow_assembly/`: assembly, annotation and binning of metaG/metaT data
  - to be executed after the preprocessing workflow
- `workflow_analysis/`: downstream analysis of the generated data
  - to be executed after all the other workflows

Directory structure of all workflows:
```
workflow_preproc/
├── envs                # snakemake conda env YAML files
│   └── <...>.yaml
├── profiles            # snakemake profiles
│   └── iris_slurm
│       ├── config.yaml
│       └── slurm.yaml
├── rules               # snakemake rules; optional
│   ├── init.smk        # shared by all workflows
│   └── <...>.smk
├── scripts             # shared or custom scripts; optional
├── README.md
└── Snakefile
```

## Other files and directories

Files which are relevant for to run the analysis are marked as such.

- `config/`
  - `COINFECTOMICS_ids.tsv`: list of sample IDs (N=125)
    - used in `scripts/samples.py`
  - `config.yaml`: main `snakemake` config file for workflows
    - to be used for all workflows (see [setup/configuration](#configuration))
    - **this file has to be adjusted to run the analysis**
  - `samples.tsv`: sample table incl. sample ID and paths to raw metaG/metaT FASTQ files
    - includes 125 samples and 1 internal control sample (`internalControlK`)
    - included in `config.yaml` (see attribute `samples`)
    - created using `scripts/samples.py` (log file: `samples.tsv.log`)
  - `template.imp3.preproc.yaml`, `template.imp3.assembly.yaml`: config templates for `imp3`
    - included in `config.yaml` (see sub-attribute `config_template` for `imp3`)
- `schemas/`:
  - schemas for validation in `snakemake` workflows
    - `config.schema.yaml`: for `config/config.yaml`
    - `samples.schema.yaml`: for `config/samples.tsv`
  - *note: these schemas are very rudimentary, and should be extended and updated*
- `scripts/`
  - `figures/`: scripts to create figures
    - mostly used in `workflow_analysis/`
  - `combine_seq_runs.sh`: script used to combine sequencing runs for some samples
    - was executed manually on the raw FASTQ files prior to the analysis
    - **there should be no need to use this script**
  - `countreads.sh`: script to count reads in (raw) FASTQ files
    - used for QC of raw FASTQ files to make sure that the paired files have the same number of reads
    - executed manually for all raw metaG/metaT FASTQ files
    - **there should be no need to use this script**
  - `pathofact_input.py`: script to create input files for `pathofact`
    - used in `workflow_assembly/`
  - `samples.py`: script to create `config/samples.tsv`
    - executed manually
    - **there should be no need to use this script**
  - `split_samples.sh`: script to split `config/samples.tsv` into chunks
    - can be used to create multiple sample batches to run the analysis only on a sample subset
    - if used, adjust `samples` in `config/config.yaml` to include the path to a created sample subset file
  - `utils.py`: `Python` utils (used in some workflows)
  - `utils.R`: `R` utils (used in `R` scripts)
- `submodules/`: `git` submodules to include other required `git` repositories
  - `imp3_assembly`: `imp3` version used for the assembly step (see `workflow_assembly/`)
  - `imp3_preproc`: `imp3` version used for the preprocessing step (see `workflow_postproc/`)
  - `pacific`: (incl. in `workflow_postproc/`, currently not used)
  - `PathoFact`: `pathofact` version used for annotation (see `workflow_assembly/`)
- `README.md`: this file
- `requirements.yaml`: main `conda` environment YAML files containing `snakemake`
  - needed to run any workflow (see [setup/conda](#conda))