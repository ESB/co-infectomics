# About

Workflow for QC of raw sequencing data files before doing any kind of preprocessing or analysis.
For example, to make sure that samples sequenced twice have higher similarity compared to other samples.

This workflow is not part of the main data analysis and there should be no need to re-run it.

```bash
# activate the main conda env.
conda activate coinf

cd workflow_qc_raw

# dry-run
snakemake --profile profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile profiles/iris_slurm
```
