# About

Workflow for downstream analysis, figures and tables.

Main steps:
- Cohort composition: sex, age, BMI distributions incl. group comparisons
- Read counts per sample from raw and processed FASTQ files
- Read counts per sample from the `kraken2` contamination analysis (PhiX/human)
- Alpha-diversity diversity estimates from `nonpareil` (metaG only) incl. group comparisons
- Analysis of data from `humann3` and `metaphlan3` with `maaslin2`
- Plotting SARS-CoV-2 qPCR results
- Plotting SARS-CoV-2 detection results from `fastv` data
- Plotting virus detection results from `fastv` data
- Alpha-diversity estimates from `motus2` data incl. group comparisons
- Beta-diversity estimated from `motus2` data
- Beta-diversity estimated from `metaphlan3` data
- Presense/absence of AROs from `rgi` profiling data incl. hierarchical clustering of samples
- Presense/absence of AROs from `rgi` data from `pathofact` incl. hierarchical clustering of samples
- Plotting assembly statistics obtained from `quast`

## Configuration and execution

**When to run:**
> To be executed after ALL the other workflows.

Config files:
- `config/config.yaml`: main config file for all workflows
- `workflow_analysis/profiles/iris_slurm/slurm.yaml`: `slurm` config
- `workflow_analysis/profiles/iris_slurm/config.yaml`: `snakemake` parameters

Before executing the workflow, check **all** config files listed above, especially lines tagged with `USER_INPUT`.

It is **not** recommended to run the workflow on the access node:
though all computation-intensive steps should be submitted via `slurm`, it is better to avoid doing that especially for (very) long jobs.

```bash
# start an interactive session

# activate the main conda env.
conda activate coinf

# dry-run
snakemake --profile workflow_analysis/profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile workflow_analysis/profiles/iris_slurm
```