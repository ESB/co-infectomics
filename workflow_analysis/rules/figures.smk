rule plot_cohort:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
    output:
        pdf="analysis/figures/cohort.pdf"
    log:
        "analysis/figures/cohort.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=10,
        height=6,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: cohort: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_cohort.R")

rule plot_read_counts:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        read_counts="qc/preproc/fastqc/{mtype}/summary.reads.tsv",
    output:
        pdf="analysis/figures/read_counts.{mtype}.pdf"
    log:
        "analysis/figures/read_counts.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=30,
        height=12,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: read counts: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_number_of_reads.R")

rule plot_read_counts_contam:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        read_counts="qc/preproc/kraken2/{mtype}/summary.phix_human.tsv",
    output:
        pdf="analysis/figures/read_counts_contam.{mtype}.pdf"
    log:
        "analysis/figures/read_counts_contam.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=30,
        height=12,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: read counts: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_kraken2_phix_human.R")

rule plot_qpcr:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv")
    output:
        pdf="analysis/figures/qpcr.pdf"
    log:
        "analysis/figures/qpcr.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=9,
        height=6,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: SARS-CoV-2 qPCR: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_qpcr.R")

rule plot_fastv_sars:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        fastv_sars="qc/preproc/fastv/mt/summary.sars-cov-2.tsv",
        bbmap_stats="sars-cov-2/mt/fastq.stats.tsv",
    output:
        pdf="analysis/figures/fastv.sars.pdf"
    log:
        "analysis/figures/fastv.sars.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=30,
        height=10,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: fastv SARS-CoV-2: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_fastv_sars.R")

rule plot_fastv_viruses:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        fastv_viruses="qc/preproc/fastv/{mtype}/summary.viruses.tsv",
    output:
        pdf="analysis/figures/fastv.viruses.{mtype}.pdf"
    log:
        "analysis/figures/fastv.viruses.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=14,
        height=12,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: fastv viruses: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_fastv_viruses.R")

rule plot_rgi_profiling_genes:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        rgi="profiling/rgi/{mtype}/joined_genes.tsv",
    output:
        pdf_hm="analysis/figures/rgi.profiling.{mtype}.genes.heatmap.pdf",
    log:
        "analysis/figures/rgi.profiling.{mtype}.genes.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        dtype=lambda wildcards: wildcards.mtype,
        width=12,
        height=8,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: RGI profiling results: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_rgi_profiling.R")

rule plot_rgi_pathofact_genes:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        rgi="pathofact/summary/rgi.notnudged.genes.tsv",
    output:
        pdf_hm="analysis/figures/rgi.pathofact.genes.heatmap.pdf",
    log:
        "analysis/figures/rgi.pathofact.genes.heatmap.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        dtype="pathofact",
        width=12,
        height=8,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: RGI profiling results: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_rgi_profiling.R")

rule plot_adiv_nonpareil:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        adiv="preproc/mg/nonpareil.mg.diversity.tsv",
    output:
        pdf="analysis/figures/adiv_nonpareil.pdf"
    log:
        "analysis/figures/adiv_nonpareil.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=10,
        height=6,
        dtype="nonpareil",
        title="Nonpareil, metaG",
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: alpha-diversity, nonpareil: {input.adiv}, {output.pdf}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_alpha_div.R")

rule plot_adiv_motus2:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        adiv="analysis/data/motus2.{mtype}.phyloseq.adiv.tsv",
    output:
        pdf="analysis/figures/adiv_motus2.{mtype}.pdf"
    log:
        "analysis/figures/adiv_motus2.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=10,
        height=6,
        dtype="motus2",
        title=lambda wildcards: "mOTUs2, {}".format(wildcards.mtype),
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: alpha-diversity, motus2: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_alpha_div.R")

rule plot_bdiv_motus2:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        phy="analysis/data/motus2.{mtype}.phyloseq.rds",
    output:
        pdf="analysis/figures/bdiv_motus2.{mtype}.pdf"
    log:
        "analysis/figures/bdiv_motus2.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=10,
        height=10,
        dtype="motus2",
        title=lambda wildcards: "mOTUs2, {}".format(wildcards.mtype),
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: beta-diversity, motus2: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_beta_div.R")

rule plot_bdiv_metaphlan3:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        phy="analysis/data/metaphlan3.{mtype}.phyloseq.rds",
    output:
        pdf="analysis/figures/bdiv_metaphlan3.{mtype}.pdf"
    log:
        "analysis/figures/bdiv_metaphlan3.{mtype}.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=10,
        height=10,
        dtype="metaphlan3",
        title=lambda wildcards: "mOTUs2, {}".format(wildcards.mtype),
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: beta-diversity, metaphlan3: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_beta_div.R")


rule plot_quast:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        quast="quast/metaquast.tsv",
    output:
        pdf="analysis/figures/quast.pdf"
    log:
        "analysis/figures/quast.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        width=8,
        height=6,
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Figures: QUAST statistics: {input}, {output}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_quast.R")