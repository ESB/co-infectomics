rule maaslin2_humann3:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        humann3_pa="profiling/humann3/{mtype}/joined_pathabundance_rab.tsv",
        humann3_gf="profiling/humann3/{mtype}/joined_genefamilies_rab.tsv",
    output:
        done="analysis/maaslin2/humann3/{mtype}/analysis.done"
    log:
        "analysis/maaslin2/humann3/{mtype}/analysis.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        odir=lambda wildcards, output: os.path.dirname(output.done)
    threads: 10
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Analysis: maaslin2 on humann3 data: {input.humann3_pa}, {input.humann3_gf}"
    script:
        os.path.join(SRC_DIR, "maaslin2", "maaslin2_humann3.R")

rule maaslin2_metaphlan3:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        metaphlan3="profiling/metaphlan3/{mtype}/joined_rab.tsv",
    output:
        done="analysis/maaslin2/metaphlan3/{mtype}/analysis.done"
    log:
        "analysis/maaslin2/metaphlan3/{mtype}/analysis.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
        odir=lambda wildcards, output: os.path.dirname(output.done)
    threads: 1
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Analysis: maaslin2 on metaphlan3 data: {input.metaphlan3}"
    script:
        os.path.join(SRC_DIR, "maaslin2", "maaslin2_metaphlan3.R")