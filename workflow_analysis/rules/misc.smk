rule phyloseq_motus2:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        motus2="assembly/motus.{mtype}.counts.tsv",
    output:
        rds="analysis/data/motus2.{mtype}.phyloseq.rds",
        adiv="analysis/data/motus2.{mtype}.phyloseq.adiv.tsv",
    log:
        "analysis/data/motus2.{mtype}.phyloseq.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Process mOTUs2 counts (phyloseq): {input.motus2}, {output}"
    script:
        os.path.join(SRC_DIR, "phyloseq", "proc_motus2.R")

rule phyloseq_metaphlan3:
    input:
        metadata=os.path.join(MDATA_DIR, "COINFECTOMICS_metadata_merged.tsv"),
        metaphlan3="profiling/metaphlan3/{mtype}/joined_rab.tsv",
    output:
        rds="analysis/data/metaphlan3.{mtype}.phyloseq.rds",
    log:
        "analysis/data/metaphlan3.{mtype}.phyloseq.log"
    params:
        utils=os.path.abspath(os.path.join(SRC_DIR, "utils.R")),
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Process MetaPhlAn3 RAB values (phyloseq): {input.metaphlan3}, {output}"
    script:
        os.path.join(SRC_DIR, "phyloseq", "proc_metaphlan3.R")


# rule kraken2_reads_report_to_tsv:
#     input:
#         "assembly/{sid}/%s/Analysis/taxonomy/kraken/{mtype}.kraken.report" % config["imp3"]["assembly"]["run"]
#     output:
#         biom=temp("analysis/data/kraken2.{sid}.{mtype}.reads.biom"),
#         tsv=temp("analysis/data/kraken2.{sid}.{mtype}.reads.tsv"),
#     wildcard_constraints:
#         sid="|".join(SAMPLES),
#         mtype="|".join(MTYPES),
#     conda:
#         os.path.join(ENV_DIR, "biom.yaml")
#     message:
#         "Reformat kraken2 reads report to TSV: {input}"
#     shell:
#         # kraken-report -> biom -> TSV
#         "kraken-biom {input} -o {output.biom} --fmt hdf5 && "
#         "biom convert -i {output.biom} -o {output.tsv} --to-tsv --header-key taxonomy && "
#         # add unclassified
#         "echo -e \"\\n0\\t$(grep -P 'U\\t0\\t\s*unclassified$' {input} | cut -f3)\\tk__; p__; c__; o__; f__; g__; s__\" >> {output.tsv}"

# def join_kraken2_reads_reports_input(wildcards):
#     sids = []
#     if wildcards.mtype == "mg":
#         sids = SIDS_MGMT
#     elif wildcards.mtype == "mt":
#         sids = SIDS_MGMT
#     return expand("analysis/data/kraken2.{sid}.{mtype}.biom", sid=sids)

# rule join_kraken2_reads_reports:
#     input:
#         join_kraken2_reads_reports_input
#     output:
#         "analysis/data/kraken2.{mtype}.reads.tsv"
#     wildcard_constraints:
#         mtype="|".join(MTYPES),
#     message:
#         "Join kraken2 TSV read reports: {output}"
#     run:
#         # TODO

# rule phyloseq_input_kraken2:
#     input:
#         expand(os.path.join(RESULTS_DIR, "{sid}.report.tsv"), sid=SAMPLES)
#     output:
#         abund=os.path.join(RESULTS_DIR, "phyloseq.kraken_abundance.tsv"),
#         tax=os.path.join(RESULTS_DIR, "phyloseq.kraken_taxonomy.tsv")
#     # wildcard_constraints:
#     #     db="|".join(config["kraken2"]["db"].keys())
#     message:
#         "Phyloseq input for Kraken2"
#     run:
#         import os
#         import re
#         import csv
#         import pandas
#         from collections import OrderedDict
        
#         from scripts.utils import read_kraken2_report2biom2tsv, lineage2dict, RANKS
#         # Required functions
# #        def read_kraken2_report2biom2tsv(ifile_path):
# #            import re
# #            from pandas import read_csv
# #            df = read_csv(
# #                ifile_path,
# #                sep="\t",
# #                comment='#',
# #                names=["taxid", "count", "lineage"]
# #            )
# #            # tax ID: to string
# #            df["taxid"] = df["taxid"].apply(lambda x: "taxid_%d" % int(x))
# #            # lineage: add "Unknown" if no taxon name and replace separator
# #            df["lineage"] = df["lineage"].apply(lambda x: re.sub("__(;|$)", "__Unknown;", x))
# #            df["lineage"] = df["lineage"].apply(lambda x: re.sub(";\s*", "|", re.sub(";$", "", x)))
# #            # lineage: put genus and species strings together
# #            df["lineage"] = df["lineage"].apply(lambda x: lineage_cat_ranks(lin=x, r1="g", r2="s"))
# #            return df
# #
# #        def lineage2dict(lin):
# #            import re
# #            if lin == "Unknown":
# #                return dict([(k, "Unknown") for k in RANKS.keys()])
# #            lin = lin.split("|")
# #            lin = [re.search("(?P<rank>^[a-zA-Z])__(?P<taxon>.*$)", taxon).groups() for taxon in lin]
# #            lin = dict(lin)
# #            return lin
# #
# #        RANKS = OrderedDict({'k': 'kingdom', 'p': 'phylum', 'c': 'class', 'o': 'order', 'f': 'family', 'g': 'genus', 's': 'species'})

#         dfs   = None
#         tax_d = dict()
#         for ifile_path in input:
#             sid = os.path.basename(ifile_path).split(".")[0]
#             df  = read_kraken2_report2biom2tsv(ifile_path)
#             df  = df.rename(columns={"count": sid})
#             df.set_index("taxid", drop=True, inplace=True, verify_integrity=True)
#             if dfs is None:
#                 dfs = df[[sid]].copy()
#                 tax_d.update(df["lineage"].to_dict())
#             else:
#                 dfs = dfs.merge(
#                     right=df[[sid]],
#                     how="outer",
#                     left_index=True, right_index=True
#                 )
#                 tax_d.update(df["lineage"].to_dict())
#         # abundance
#         df_a = dfs.copy()
#         # taxonomy
#         df_t = pandas.DataFrame(index=tax_d.keys(), columns=RANKS.values())
#         for t_id, t_lin in tax_d.items():
#             for t_r, t_n in lineage2dict(t_lin).items():
#                 df_t.loc[t_id, RANKS[t_r]] = t_n
#         # save
#         df_a.to_csv(output.abund, sep="\t", header=True, index=True, index_label="taxonID", na_rep=0)
#         df_t.to_csv(output.tax, sep="\t", header=True, index=True, index_label="taxonID")
