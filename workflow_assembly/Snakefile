##################################################
# INIT

include:
    "rules/init.smk"

# multi-omics data type
MTYPES = ["mt", "mg"]

# read IDs for FASTQ files
RIDS   = ["r1", "r2", "se"]
RIDS_2 = ["pe", "se"]

# Sample IDs w/ MG/MT data
SIDS_MG   = SAMPLES.loc[(pandas.notnull(SAMPLES.mg_R1)) & (pandas.notnull(SAMPLES.mg_R2))].index
SIDS_MT   = SAMPLES.loc[(pandas.notnull(SAMPLES.mt_R1)) & (pandas.notnull(SAMPLES.mt_R2))].index
SIDS_MGMT = SAMPLES.loc[
    (pandas.notnull(SAMPLES.mg_R1)) &
    (pandas.notnull(SAMPLES.mg_R2)) &
    (pandas.notnull(SAMPLES.mt_R1)) &
    (pandas.notnull(SAMPLES.mt_R2))
].index

##################################################
# RULES

include:
    "rules/assembly.smk"

include:
    "rules/pathofact.smk"

include:
    "rules/misc.smk"

localrules: all
rule all:
    input:
        # metaG/metaT assembly
        assembly=expand(
            "assembly/{sid}/{runid}/status/workflow.done",
            sid=SIDS_MGMT,
            runid=config["imp3"]["preprocessing"]["run"],
        ),
        # quast
        quast="quast/metaquast.tsv",
        # pathofact
        pathofact=expand(
            "pathofact/{sid}/pf_out/PathoFact_report/PathoFact_{sid}_predictions.tsv", # one of the main output files
            sid=SIDS_MGMT,
        ),
        pathofact_summary=[
            "pathofact/summary/rgi.notnudged.genes.tsv",
        ],
        # summaries
        motus=expand("assembly/motus.{mtype}.counts.tsv", mtype=MTYPES),
