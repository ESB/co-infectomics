# About

Workflow for *de novo* metaG/metaT assembly and further downstream analysis such as annotation and binning.

Main steps:
- run `imp3` (per sample)
  - using prepared `imp3` config file
  - steps: `assembly`, `analysis`, `binning`, `taxonomy`
- run `pathofact` (per sample)
  - using prepared `pathofact`
- run `metaquast` (per sample)
- create summaries:
  - joined `quast` statistics
  - joined `motus2` count table
  - joined `rgi` presence/absence table of AROs from `rgi` (from `pathofact` output)

*TODO: to be updated/expanded (e.g., other summaries from `pathofact` output).*

## Configuration and execution

**When to run:**
> To be executed after the preprocessing workflow.

**IMPORTANT**
> Execute the workflow first using **ONE** sample only!!!

**IMPORTANT**
> `DeepVirFinder` (used in `pathofact`) can sometimes get stuck resulting in an idle process.
> Restarting the sample after removing its output folder should solve the issue.

Config files:
- `config/config.yaml`: main config file for all workflows
- `workflow_assembly/profiles/iris_slurm/slurm.yaml`: `slurm` config
- `workflow_assembly/profiles/iris_slurm/config.yaml`: `snakemake` parameters

Before executing the workflow, check **all** config files listed above, especially lines tagged with `USER_INPUT`.

It is **not** recommended to run the workflow on the access node:
though all computation-intensive steps should be submitted via `slurm`, it is better to avoid doing that especially for (very) long jobs.

```bash
# start an interactive session

# activate the main conda env.
conda activate coinf

# dry-run
snakemake --profile workflow_assembly/profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile workflow_assembly/profiles/iris_slurm
```

# Notes

## `imp3`

- [repository](https://git-r3lab.uni.lu/IMP/imp3)
- [documentation](https://imp3.readthedocs.io/en/impiris/)

To see which version was used for preprocessing:
```bash
cd submodules/imp3_preproc/
git describe --always
```

## `pathofact`

- [repository](https://gitlab.lcsb.uni.lu/laura.denies/PathoFact)

To see which version was used for preprocessing:
```bash
cd submodules/PathoFact/
git describe --always
```