##################################################
# Assembly w/ IMP3

localrules: imp3_assembly_conf

rule imp3_assembly_conf:
    input:
        mg=expand(
            "preproc/mg/{{sid}}/{{runid}}/Preprocessing/mg.{rid}.preprocessed.fq",
            rid=RIDS
        ),
        mt=expand(
            "preproc/mt/{{sid}}/{{runid}}/Preprocessing/mt.{rid}.preprocessed.fq",
            rid=RIDS
        )
    output:
        temp("assembly/{sid}.assembly.{runid}.imp3.yaml")
    wildcard_constraints:
        sid="|".join(SIDS_MGMT),
        runid=config["imp3"]["assembly"]["run"],
    params:
        config_template=IMP3_TEMPLATE_ASM
    run:
        import yaml
        with open(params.config_template, "r") as ifile, open(output[0], "w") as ofile:
            conf = yaml.full_load(ifile)
            # set parameters
            conf["raws"]["Metagenomics"] = " ".join([os.path.abspath(f) for f in input.mg])
            conf["raws"]["Metatranscriptomics"] = " ".join([os.path.abspath(f) for f in input.mt])
            conf["sample"] = wildcards.sid
            conf["outputdir"] = os.path.join("assembly", wildcards.sid, wildcards.runid) # NOTE: look at output from assembly rule
            conf["db_path"] = config["imp3"]["assembly"]["db_path"]
            for key in conf["mem"].keys():
                conf["mem"][key] = config["imp3"]["assembly"]["mem"][key]
            # save
            yaml.dump(conf, ofile)

rule imp3_assembly_mgmt:
    input:
        fastq=expand(
            "preproc/{mtype}/{{sid}}/{{runid}}/Preprocessing/{mtype}.{rid}.preprocessed.fq",
            mtype=MTYPES, rid=RIDS
        ),
        conf="assembly/{sid}.assembly.{runid}.imp3.yaml",
    output:
        done="assembly/{sid}/{runid}/status/workflow.done",
        contigs="assembly/{sid}/{runid}/Assembly/mgmt.assembly.merged.fa",
        faa="assembly/{sid}/{runid}/Analysis/annotation/prokka.faa",
        fna="assembly/{sid}/{runid}/Analysis/annotation/prokka.fna",
        gff="assembly/{sid}/{runid}/Analysis/annotation/annotation_CDS_RNA_hmms.gff",
        # annot2contig="assembly/{sid}/{runid}/Analysis/annotation/annotation.filt.contig2ID.tsv",
        motus=expand("assembly/{{sid}}/{{runid}}/Analysis/taxonomy/mOTUs/{mtype}.mOTU.counts.tsv", mtype=MTYPES),
    log:
        "assembly/{sid}/{runid}/smk.log"
    wildcard_constraints:
        sid="|".join(SIDS_MGMT),
        runid=config["imp3"]["assembly"]["run"],
    params:
        tmpdir="/tmp", # this path should not be too long because of https://git-r3lab.uni.lu/IMP/imp3/-/issues/54
        smk=os.path.abspath(os.path.join(MOD_DIR, "imp3_assembly", "Snakefile")), # NOTE: a diff. version as the one used for preprocessing
        conda_dir=os.path.abspath(config["imp3"]["assembly"]["conda_dir"]),
    threads:
        config["imp3"]["assembly"]["mem"]["big_mem_cores"] # !!! run on bigmem
    conda:
        os.path.join(ENV_DIR, "snakemake.yaml")
    message:
        "MG/MT Assembly w/ IMP3: {wildcards.sid}, {wildcards.runid}"
    shell:
        "TMPDIR={params.tmpdir} snakemake -s {params.smk} -rp --rerun-incomplete --cores {threads} --configfile {input.conf} --use-conda --conda-prefix {params.conda_dir} &> {log}"

##################################################
# Assembly analysis

# metaQUAST
# http://quast.sourceforge.net
rule metaquast:
    input:
        "assembly/{{sid}}/{runid}/Assembly/mgmt.assembly.merged.fa".format(runid=config["imp3"]["assembly"]["run"])
    output:
        tsv="quast/{sid}/report.tsv",
    wildcard_constraints:
        sid="|".join(SIDS_MGMT),
    params:
        # http://quast.sourceforge.net/docs/manual.html
        # do not use any reference genomes;
        # min contig length (affects some stats); contig length cutoffs (for some stats)
        params=lambda wildcards: "--max-ref-number 0 --min-contig 1000 --contig-thresholds 0,1000,2000,5000 -l \'{}\'".format(wildcards.sid)
    conda:
        os.path.join(ENV_DIR, "quast.yaml")
    message:
        "Run metaQUAST on assembly {input}"
    shell:
        "metaquast -o $(dirname {output[0]}) -t {threads} {params.params} {input}"
