##################################################
# Annotation with PathoFact
# https://git-r3lab.uni.lu/laura.denies/PathoFact
# NOTE: used RGI version in PathoFact cannot handle stop codons (symbols "*")

##################################################
# PathoFact input

localrules: pathofact_fna
rule pathofact_fna:
    input:
        fna="assembly/{sid}/%s/Analysis/annotation/prokka.fna" % config["imp3"]["assembly"]["run"],
    output:
        fna=temp("pathofact/{sid}/{sid}.fna")
    message:
        "PathoFact input FASTA: {output}"
    shell:
        "rsync -rv {input.fna} {output.fna}"

# NOTE: RGI (used in PathoFact) can throw an error if there are any unexpected AA symbols, e.g. "*"
# localrules: pathofact_faa_map
rule pathofact_faa_map:
    input:
        faa="assembly/{sid}/%s/Analysis/annotation/prokka.faa" % config["imp3"]["assembly"]["run"],
        gff="assembly/{sid}/%s/Analysis/annotation/annotation_CDS_RNA_hmms.gff" % config["imp3"]["assembly"]["run"],
        # tsv="assembly/{sid}/%s/Analysis/annotation/annotation.filt.contig2ID.tsv" % config["imp3"]["assembly"]["run"],
    output:
        faa="pathofact/{sid}/{sid}.faa",
        tsv="pathofact/{sid}/{sid}.contig",
    log:
        "pathofact/{sid}/{sid}.faa.log"
    conda:
        os.path.join(ENV_DIR, "biopython.yaml")
    message:
        "PathoFact input: {output} from {input}"
    script:
        os.path.join(SRC_DIR, "pathofact_input.py")

##################################################
# PathoFact config

localrules: pathofact_conf
rule pathofact_conf:
    input:
        fna=rules.pathofact_fna.output.fna,
        faa=rules.pathofact_faa_map.output.faa,
        fna2faa=rules.pathofact_faa_map.output.tsv,
    output:
        conf="pathofact/{sid}/{sid}.yaml"
    wildcard_constraints:
        sid="|".join(SAMPLES.index),
    params:
        smk=os.path.abspath(os.path.join(MOD_DIR, "pathofact")),
        params=config["pathofact"],
        # paths=["scripts", "deepvirfinder", "tox_hmm", "tox_lib", "vir_hmm", "vir_domains"],
    message:
        "PathoFact config for {wildcards.sid}"
    run:
        import yaml
        with open(output.conf, "w") as ofile:
            conf = {"pathofact": params.params}
            # set parameters
            conf["pathofact"]["sample"]  = f"{wildcards.sid}"
            conf["pathofact"]["datadir"] = os.path.abspath(os.path.dirname(output.conf))
            # save
            yaml.dump(conf, ofile)

##################################################
# PathoFact analysis

rule pathofact:
    input:
        fna=rules.pathofact_fna.output.fna,
        faa=rules.pathofact_faa_map.output.faa,
        fna2faa=rules.pathofact_faa_map.output.tsv,
        conf=rules.pathofact_conf.output.conf,
    output:
        paf="pathofact/{sid}/pf_out/PathoFact_report/PathoFact_{sid}_predictions.tsv",
        amr="pathofact/{sid}/pf_out/PathoFact_report/AMR_MGE_prediction_{sid}_report.tsv",
        toxg="pathofact/{sid}/pf_out/PathoFact_report/Toxin_gene_library_{sid}_report.tsv",
        toxp="pathofact/{sid}/pf_out/PathoFact_report/Toxin_prediction_{sid}_report.tsv",
        vir="pathofact/{sid}/pf_out/PathoFact_report/Virulence_prediction_{sid}_report.tsv",
        rgi="pathofact/{sid}/pf_out/PathoFact_intermediate/PathoFact_intermediate/AMR/RGI_results/{sid}.RGI.txt",
    log:
        os.path.join("pathofact/{sid}/pf_out/{sid}.smk.log"),
    wildcard_constraints:
        sid="|".join(SAMPLES.index),
    params:
        smk=os.path.abspath(os.path.join(MOD_DIR, "PathoFact")),
        conda_dir=os.path.abspath(config["imp3"]["assembly"]["conda_dir"]), # same as for IMP3
    threads: 10
    conda:
        os.path.join(ENV_DIR, "snakemake_pathofact.yaml")
    message:
        "Run PathoFact for {wildcards.sid}"
    shell:
        "logf=$(realpath {log}) && "
        "conf=$(realpath {input.conf}) && "
        "cd {params.smk} && "
        # sleep a random number of seconds before starting pathofact:
        # assuming that jobs starting at the same time might be causing
        # snakemake issues when locking the working directory
        "sleep $[ ( $RANDOM % 30 )  + 1 ]s && "
        "snakemake -s Snakefile -rp --cores {threads} --configfile ${{conf}} --use-conda --conda-prefix {params.conda_dir} &> ${{logf}}"

##################################################
# PathoFact summaries

# RGI presence/absence matrix
# NOTE: nudged hits are excluded
rule pathofact_join_rgi_pa:
    input:
        expand(
            "pathofact/{sid}/pf_out/PathoFact_intermediate/PathoFact_intermediate/AMR/RGI_results/{sid}.RGI.txt",
            sid=SIDS_MGMT
        )
    output:
        genes="pathofact/summary/rgi.notnudged.genes.tsv"
    message:
        "RGI: profiling: genes found in filtered PE/SE hits: all samples"
    run:
        # sample ID from input file name
        def get_sid_from_file(fname):
            return re.sub("\.RGI\.txt", "", os.path.basename(fname))
        # get genes from all hits
        def read_genes(fname):
            fdata = pandas.read_csv(ifile, sep="\t", header=0, dtype='str')
            fdata = fdata.loc[(fdata.ORF_ID != "ORF_ID") & (fdata.Nudged != "True"),:]
            return set(["ARO:{}".format(aro) for aro in fdata.ARO])
        # AROs
        aros = set()
        for ifile in input:
            aros = aros.union(read_genes(ifile))
        # fill
        genes = pandas.DataFrame(0, index=aros, columns=[get_sid_from_file(ifile) for ifile in input])
        for ifile in input:
            genes.loc[read_genes(ifile),get_sid_from_file(ifile)] = 1 # present
        # save
        genes.to_csv(output.genes, sep="\t", header=True, index=True, index_label="ARO")