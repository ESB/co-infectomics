rule join_motus:
    input:
        expand(
            "assembly/{sid}/{runid}/Analysis/taxonomy/mOTUs/{{mtype}}.mOTU.counts.tsv",
            sid=SIDS_MGMT, runid=config["imp3"]["assembly"]["run"]
        )
    output:
        counts="assembly/motus.{mtype}.counts.tsv"
    wildcard_constraints:
        mtype="|".join(MTYPES)
    message:
        "Join mOTUs tables: {output}"
    run:
        joined_counts = None
        for fname in input:
            # sample ID
            sid = os.path.normpath(fname).split(os.path.sep)[1]
            if joined_counts is not None:
                assert sid not in joined_counts.columns
            # counts
            counts = pandas.read_csv(fname, sep="\t", header=0)
            counts.rename(columns={"unnamed sample": sid}, inplace=True)
            counts.set_index(keys="#mOTUs2_clade", drop=True, inplace=True, verify_integrity=True)
            # join
            if joined_counts is None:
                joined_counts = counts.copy()
            else:
                joined_counts = pandas.merge(
                    left=joined_counts, right=counts,
                    how="outer",
                    left_index=True, right_index=True
                )
        # save
        joined_counts.to_csv(
            output.counts,
            sep="\t",
            na_rep=0, # replace NAs by 0 counts
            header=True, index=True, index_label="motus2_clade"
        )

rule join_metaquast:
    input:
        expand(
            "quast/{sid}/report.tsv",
            sid=SIDS_MGMT, runid=config["imp3"]["assembly"]["run"]
        )
    output:
        summary="quast/metaquast.tsv"
    message:
        "metaQUAST summary"
    run:
        summary = None
        for fname in input:
            stats = pandas.read_csv(fname, sep="\t", header=0, index_col="Assembly")
            # join
            if summary is None:
                summary = stats.copy()
            else:
                summary = pandas.merge(
                    left=summary, right=stats,
                    how="inner",
                    left_index=True, right_index=True
                )
        # save
        summary.to_csv(output.summary, sep="\t", header=True, index=True, index_label="Statistic")
