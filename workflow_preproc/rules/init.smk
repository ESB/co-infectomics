# Initialization of a snakemake workflow
# Do not include here variables/settings which should/cannot be shared by all workflows

##################################################
# MODULES

import os
import re
import pandas
from snakemake.utils import validate

##################################################
# CONFIG

# Config validation
validate(config, srcdir("../../schemas/config.schema.yaml"))
# Sample table (tab-separated, w/ header, 1st column is sample ID)
SAMPLES = pandas.read_csv(config["samples"], header=0, sep="\t").set_index("Sample_ID", drop=False)
# Sample table validation
validate(SAMPLES, srcdir("../../schemas/samples.schema.yaml"))

##################################################
# PATHS

SRC_DIR = srcdir("../../scripts") # add. scripts
ENV_DIR = srcdir("../envs") # conda env. yaml files
MOD_DIR = srcdir("../../submodules") # git submodules
DBS_DIR = config["dbsdir"] # database folder

MDATA_DIR = os.path.abspath(config["metadata"]) # path to (meta)data

SAMPLES_FILE = os.path.abspath(config["samples"]) # samples: IDs and raw metaG/metaT FASTQ files

IMP3_TEMPLATE_PREPROC = os.path.abspath(config["imp3"]["preprocessing"]["config_template"]) # IMP3 template for preprocessing
IMP3_TEMPLATE_ASM     = os.path.abspath(config["imp3"]["assembly"]["config_template"]) # IMP3 template for assembly

OLDPWD = os.path.abspath(os.getcwd()) # PWD before changing the working directory

##################################################
# EXECUTION

# default executable for snakmake
shell.executable("bash")

# working directory
workdir:
    config["workdir"]
