##################################################
# IMP3 input

localrules: imp3_input_mt

rule imp3_input_mt:
    input:
        r1=lambda wildcards: SAMPLES.loc[wildcards.sid, "mt_R1"],
        r2=lambda wildcards: SAMPLES.loc[wildcards.sid, "mt_R2"],
    output:
        r1=temp("preproc/input/{sid}.mt.r1.fq.gz"),
        r2=temp("preproc/input/{sid}.mt.r2.fq.gz"),
    wildcard_constraints:
        sid="|".join(SAMPLES.index),
    message:
        "Input for IMP3 preprocessing: {wildcards.sid}, MT"
    shell:
        "ln -sv $(realpath {input.r1}) {output.r1} && "
        "ln -sv $(realpath {input.r2}) {output.r2}"

# some reads have 151 bases instead of 150 --> remove 151th base
rule imp3_input_mg:
    input:
        r1=lambda wildcards: SAMPLES.loc[wildcards.sid, "mg_R1"],
        r2=lambda wildcards: SAMPLES.loc[wildcards.sid, "mg_R2"],
    output:
        r1=temp("preproc/input/{sid}.mg.r1.fq.gz"),
        r2=temp("preproc/input/{sid}.mg.r2.fq.gz"),
        r1_tmp=temp("preproc/input/{sid}.mg.r1.fq.tmp"),
        r2_tmp=temp("preproc/input/{sid}.mg.r2.fq.tmp"),
    log:
        "preproc/input/{sid}.mg.log"
    wildcard_constraints:
        sid="|".join(SAMPLES.index),
    threads:
        config["cutadapt"]["threads"]
    conda:
        os.path.join(ENV_DIR, "cutadapt.yaml")
    message:
        "Input for IMP3 preprocessing: {wildcards.sid}, MG"
    shell:
        # decompress into tmp files because of spuriors errors ("gzip.BadGzipFile: CRC check failed")
        # though the gzipped FASTQ files seem not to be okay and not corrupted
        "pigz -d -c -p {threads} {input.r1} > {output.r1_tmp} && "
        "pigz -d -c -p {threads} {input.r2} > {output.r2_tmp} && "
        # https://cutadapt.readthedocs.io/en/stable/guide.html#shortening-reads-to-a-fixed-length
        "cutadapt -j {threads} --length 150 -o {output.r1} -p {output.r2} {output.r1_tmp} {output.r2_tmp} &> {log}"

##################################################
# Preprocessing w/ IMP3

localrules: imp3_preproc_conf

rule imp3_preproc_conf:
    input:
        r1="preproc/input/{sid}.{mtype}.r1.fq.gz",
        r2="preproc/input/{sid}.{mtype}.r2.fq.gz",
    output:
        temp("preproc/{mtype}/{sid}.preproc.{runid}.imp3.yaml")
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
        runid=config["imp3"]["preprocessing"]["run"],
    params:
        config_template=IMP3_TEMPLATE_PREPROC
    run:
        import yaml
        with open(params.config_template, "r") as ifile, open(output[0], "w") as ofile:
            conf = yaml.full_load(ifile)
            # set parameters
            if wildcards.mtype == "mg":
                conf["raws"]["Metagenomics"] = " ".join([os.path.abspath(f) for f in input])
                del conf["raws"]["Metatranscriptomics"]
            elif wildcards.mtype == "mt":
                conf["raws"]["Metatranscriptomics"] = " ".join([os.path.abspath(f) for f in input])
                del conf["raws"]["Metagenomics"]
            conf["sample"] = wildcards.sid
            conf["outputdir"] = os.path.join("preproc", wildcards.mtype, wildcards.sid, wildcards.runid)
            conf["db_path"] = config["imp3"]["preprocessing"]["db_path"]
            for key in conf["mem"].keys():
                conf["mem"][key] = config["imp3"]["preprocessing"]["mem"][key]
            # save
            yaml.dump(conf, ofile)

rule imp3_preproc:
    input:
        r1="preproc/input/{sid}.{mtype}.r1.fq.gz",
        r2="preproc/input/{sid}.{mtype}.r2.fq.gz",
        conf="preproc/{mtype}/{sid}.preproc.{runid}.imp3.yaml",
    output:
        r1="preproc/{mtype}/{sid}/{runid}/Preprocessing/{mtype}.r1.preprocessed.fq",
        r2="preproc/{mtype}/{sid}/{runid}/Preprocessing/{mtype}.r2.preprocessed.fq",
        se="preproc/{mtype}/{sid}/{runid}/Preprocessing/{mtype}.se.preprocessed.fq",
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    params:
        tmpdir=lambda wildcards, output: os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(output.r1)), "tmp")),
        smk=os.path.abspath(os.path.join(MOD_DIR, "imp3_preproc", "Snakefile")),
        conda_dir=os.path.abspath(config["imp3"]["preprocessing"]["conda_dir"]),
    threads:
        config["imp3"]["preprocessing"]["mem"]["big_mem_cores"] # !!! run on bigmem
    conda:
        os.path.join(ENV_DIR, "snakemake.yaml")
    message:
        "Preprocessing w/ IMP3: {wildcards.sid}, {wildcards.mtype}"
    shell:
        "TMPDIR={params.tmpdir} snakemake -s {params.smk} -rp --rerun-incomplete --cores {threads} --configfile {input.conf} --use-conda --conda-prefix {params.conda_dir}"
