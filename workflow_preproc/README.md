# About

Workflow for preprocessing raw metaG/metaT sequencing data: quality and adapter trimming, and filtering out contaminants.

Main steps:
- prepare input FASTQ files
  - metaG: trimming raw reads with more than 150 bases (see notes below)
  - metaT: symlinks to raw FASTQ files
- run `imp3` for preprocessing (per sample and metaG/metaT)
  - using prepared `imp3` config file

## Configuration and execution

**When to run:**
> To be executed as the first workflow.

**IMPORTANT**
> Execute the workflow first using **ONE** sample only!!!

Config files:
- `config/config.yaml`: main config file for all workflows
- `workflow_preproc/profiles/iris_slurm/slurm.yaml`: `slurm` config
- `workflow_preproc/profiles/iris_slurm/config.yaml`: `snakemake` parameters

Before executing the workflow, check **all** config files listed above, especially lines tagged with `USER_INPUT`.

It is **not** recommended to run the workflow on the access node:
though all computation-intensive steps should be submitted via `slurm`, it is better to avoid doing that especially for (very) long jobs.

```bash
# start an interactive session

# activate the main conda env.
conda activate coinf

# dry-run
snakemake --profile workflow_preproc/profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile workflow_preproc/profiles/iris_slurm
```

# Notes

## Metagenomics sequencing data

Some reads maybe be 151 bases instead of max. 150 bases.
In these cases, the last base should be trimmed.
Therefore, instead of using the raw FASTQ files for preprocessing with `imp3` the reads are first trimmed to max. of 150 bases.

## `imp3`

- [repository](https://git-r3lab.uni.lu/IMP/imp3)
- [documentation](https://imp3.readthedocs.io/en/impiris/)

To see which version was used for preprocessing:
```bash
cd submodules/imp3_preproc/
git describe --always
```

### Read filtering to remove contaminants

**Genome-based filtering** (metaG/metaT reads)
- PhiX genome: `gi|9626372|ref|NC_001422.1`, `Enterobacteria phage phiX174 sensu lato, complete genome`
- hg38 genome

> Note: A copy of these genomes was saved as a backup and can be made available if required.

**rRNA filtering** (metaT reads only)
- source: [SortMeRNA repository](https://github.com/biocore/sortmerna/tree/master/data/rRNA_databases)
- version `v4.2.0-238-g90cdf6c`
```
c0cd2aa2e84e3e3977859c34feb63cd5  rfam-5.8s-database-id98.fasta
703e4c270ab0a578deb4800c33b36367  rfam-5s-database-id98.fasta
8b4e6c6f17f6f35444a60fdc915e052c  silva-arc-16s-id95.fasta
ca4edcdddb98d7868f93e2308e297704  silva-arc-23s-id98.fasta
db6e72022cf650c4b33bd888b92a0391  silva-bac-16s-id90.fasta
f347d2f8f8ffbfa28c785e3a9fe3db79  silva-bac-23s-id98.fasta
878a413765d09c3ec75409fb1d1573f1  silva-euk-18s-id95.fasta
cbb973e63f52981bd591de0404df5839  silva-euk-28s-id98.fasta
```

> Note: A copy of the the FASTA files and their index files was saved as a backup and can be made available if required.