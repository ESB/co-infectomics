# Python utils

from collections import OrderedDict

FASTQ_EXT = '.fastq.gz'
FASTQ_EXT_RE = '\.fastq\.gz$'

RANKS = OrderedDict({'k': 'kingdom', 'p': 'phylum', 'c': 'class', 'o': 'order', 'f': 'family', 'g': 'genus', 's': 'species'})

def setup_logger(logfile=None):
    """
    Setup a logger
    """
    import logging

    # create logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # formatter
    logform = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # console handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logform)
    logger.addHandler(ch)

    # file handler
    if logfile:
        fh = logging.FileHandler(logfile)
        fh.setFormatter(logform)
        logger.addHandler(fh)
    return

def basenamefromFASTQ(fname, ext_re=FASTQ_EXT_RE):
    """
    get basename of a FASTQ file w/o the read ID (R1/R2)
    """
    import os, re
    fname = re.sub(ext_re, '', os.path.basename(fname))
    assert re.search('_R\d_\d+$', fname), 'Cannot find expected read ID pattern in {}'.format(fname)
    fname = re.sub('_R\d_\d+$', '', fname)
    return fname


def sampleIDfromFASTQ(fname, ext_re=FASTQ_EXT_RE):
    """
    get sample ID from raw FASTQ file name
    """
    import os, re
    fname = re.sub(ext_re, '', os.path.basename(fname))
    # samples
    if re.search('^COINF_PRED\d{4}_MT_COINF_ESB_(S\d+)+_R\d_\d+$', fname):
        return 'COINF_{}'.format(re.search('_(?P<sid>PRED\d{4})_', fname).group('sid'))
    elif re.search('^CONV\d{3}_(RNA|DNA)_COINF_\d{8}_CMG_COINF_ESB_(S\d+)+_R\d_\d+$', fname):
        return re.sub('CONV', 'CONV_', re.search('^(?P<sid>CONV\d{3})_', fname).group('sid'))
    elif re.search('^PRED\d{4}_(RNA|DNA)_COINF_\d{8}_CMG_COINF_ESB_(S\d+)+_R\d_\d+$', fname):
        return 'COINF_{}'.format(re.search('^(?P<sid>PRED\d{4})_', fname).group('sid'))
    # exceptions
    elif re.search('^internalControlK_(RNA|DNA)_COINF_\d{8}_CMG_COINF_ESB_(S\d+)+_R\d_\d+$', fname):
        return 'internalControlK'
    # should not happen
    else:
        raise Exception('Unexpected FASTQ file name pattern: {}'.format(fname))

def readIDfromFASTQ(fname, ext_re=FASTQ_EXT_RE):
    """
    get read ID (R1/R2) from raw FASTQ file name
    """
    import os, re
    fname = re.sub(ext_re, '', os.path.basename(fname))
    if re.search('_(S\d+)+_R1_\d+$', fname):
        return "R1"
    elif re.search('_(S\d+)+_R2_\d+$', fname):
        return "R2"
    else:
        raise Exception('Unexpected FASTQ file name pattern: {}'.format(fname))

def is_paired(fastqs):
    """
    Check if given list of two FASTQ files are paired FASTQ files
    """
    import os
    # must be two files
    if len(fastqs) != 2:
        return False
    # R1/R2
    rids = [readIDfromFASTQ(f) for f in fastqs]
    if sorted(rids) != ['R1', 'R2']:
        return False
    # same basenames
    fastqs = sorted([os.path.join(os.path.dirname(fname), basenamefromFASTQ(fname)) for fname in fastqs])
    if len(set(fastqs)) != 1:
        return False
    return True

def read_kraken2_report(fname):
    """
    Read in and process a kraken2 report file
    Official manual: https://github.com/DerrickWood/kraken2/wiki/Manual#sample-report-output-format
    """
    import re
    import pandas
    # read in
    df = pandas.read_csv(fname, sep='\t', header=None, names=['pct','count','count_direct','rank','tax_id', "tax_name"])
    # rm leading whispaces from tax. names
    df['tax_name'] = df['tax_name'].apply(lambda x: re.sub('^\s+','',x))
    return df

def read_kraken2_report2biom2tsv(ifile_path):
    """Read kraken2 report which was converted to TSV format"""
    import re
    from pandas import read_csv
    df = read_csv(ifile_path, sep="\t", comment='#', names=["taxid", "count", "lineage"])
    # tax ID: to string
    df["taxid"] = df["taxid"].apply(lambda x: "taxid_%d" % int(x))
    # lineage: add "Unknown" if no taxon name and replace separator
    df["lineage"] = df["lineage"].apply(lambda x: re.sub("__(;|$)", "__Unknown;", x))
    df["lineage"] = df["lineage"].apply(lambda x: re.sub(";\s*", "|", re.sub(";$", "", x)))
    # lineage: put genus and species strings together
    df["lineage"] = df["lineage"].apply(lambda x: lineage_cat_ranks(lin=x, r1="g", r2="s"))
    return df

def lineage_cat_ranks(lin, r1="g", r2="s"):
    """XXX"""
    # to concat taxon names from different ranks, e.g. genus + species
    # lin: unprocessed lineage string: <rank>_<name>, separated by "|"
    import re
    lin   = lin.split("|")
    lin   = [re.search("(?P<rank>^[a-zA-Z])__(?P<taxon>.*$)", taxon).groups() for taxon in lin]
    lin_d = dict(lin)
    if lin_d[r2] and lin_d[r2] != "Unknown":
        lin_d[r2] = lin_d[r1] + " " + lin_d[r2]
    return "|".join([t[0] + "__" + lin_d[t[0]] for t in lin])

def lineage2dict(lin):
    """XXX"""
    import re
    if lin == "Unknown":
        return dict([(k, "Unknown") for k in RANKS.keys()])
    lin = lin.split("|")
    lin = [re.search("(?P<rank>^[a-zA-Z])__(?P<taxon>.*$)", taxon).groups() for taxon in lin]
    lin = dict(lin)
    return lin

def get_humann3_db_type(wildcards):
    if wildcards.db == "chocophlan":
        return "nucleotide"
    elif wildcards.db == "uniref":
        return "protein"

def read_rgi_profiling_gene_report(fname):
    """
    Read in and process an RGI read profiling gene report *.gene_mapping_data.txt
    """
    import pandas
    df = pandas.read_csv(fname, sep="\t", header=0, dtype={"ARO Accession": 'str'})
    # add prefix to ARO IDs so they are not confused with numbers
    df["ARO Accession"] = "ARO:" + df["ARO Accession"]
    return df