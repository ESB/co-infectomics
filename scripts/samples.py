#!/usr/bin/env python

# NOTE: Script to create config/samples.tsv from a list of sample IDs and paths containing FASTQ files.
#   Exceptions, e.g. samples without sequencing data etc., are defined below under "SAMPLE EXCEPTIONS"
# python3 scripts/samples.py \
#     --fastq_mg /work/projects/co-infectomics/raw/metag/fastq \
#     --fastq_mt /work/projects/co-infectomics/raw/metat/fastq \
#     --fastq_mt_comb /work/projects/co-infectomics/raw/metat_combined/fastq \
#     --fastqc_mg /mnt/isilon/projects/co-infectomics/analysis/qc/raw/fastqc/mg/multiqc_data/multiqc_fastqc.txt \
#     --fastqc_mt /mnt/isilon/projects/co-infectomics/analysis/qc/raw/fastqc/mt/multiqc_data/multiqc_fastqc.txt \
#     --tsv config/samples.tsv

##################################################
# MODULES
##################################################
import os
import re
import glob
import numpy
import pandas
import logging
import argparse

from utils import setup_logger, sampleIDfromFASTQ, readIDfromFASTQ, FASTQ_EXT, FASTQ_EXT_RE, is_paired

##################################################
# SAMPLE EXCEPTIONS
##################################################
# if sequencing is complete
SAMPLES_COMPLETE = {
    'mg': False,
    'mt': True,
}
# samples without FASTQ files
SAMPLES_NA = {
    'mg': [
        # TODO
    ],
    'mt': [
        # failed during library prep
        'CONV_013', 'CONV_036', 'CONV_054', 'COINF_PRED0357', 'COINF_PRED0866', 'COINF_PRED1246',
        # forgotten
        'COINF_PRED1059',
    ],
}
# samples with mult. seq. runs
SAMPLES_MULT = {
    'mg': [
        # TODO
    ],
    'mt': [
        # included by accident instead of COINF_PRED1059
        'CONV_050',
        # sequenced twice because of a lower seq. depth than planned
        'COINF_PRED0280', 'COINF_PRED1167',
        # re-sequenced because they were previously sequenced in a test run
        'COINF_PRED0010', 'COINF_PRED0042', 'COINF_PRED0119', 'COINF_PRED0140'
    ],
}
# samples with combined seq. runs
SAMPLES_COMB = {
    'mg': [
        # TODO
    ],
    'mt': [
        # sequenced twice because of a lower seq. depth than planned; runs can be combined
        'COINF_PRED0280', 'COINF_PRED1167'
    ],
}

##################################################
# VARS and FUNCS
##################################################

def get_read_count(x, fastqc):
    bname = re.sub('_\d+\.fastq\.gz$', '', os.path.basename(x['FASTQ']))
    try:
        return fastqc[x['Sequencing']].loc['{}_{}'.format(x['Sequencing'], bname),'Total Sequences']
    except KeyError:
        return None

def get_sample_fastqs_with_most_reads(df):
    # assumptions: same sample, seq. and seq. run
    # sort by read count and get top 2 hits, i.e. paired FASTQ 
    df = df.sort_values(by='Read_Count', ascending=False).head(2)
    return df['FASTQ'].values

##################################################
# MAIN
##################################################
if __name__ == "__main__":

    # args
    descr = 'Script to create config/samples.tsv'
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument('--fastq_mg', help='directories with metaG FASTQ files (*.fastq.gz)', required=True, nargs='+')
    parser.add_argument('--fastq_mt', help='directories with metaT FASTQ files (*.fastq.gz)', required=True, nargs='+')
    parser.add_argument('--fastq_mg_comb', help='directories with COMBINED metaG FASTQ files (*.fastq.gz)', nargs='*', default=[])
    parser.add_argument('--fastq_mt_comb', help='directories with COMBINED metaT FASTQ files (*.fastq.gz)', nargs='*', default=[])
    parser.add_argument('--ids', help='sample IDs list', default='config/COINFECTOMICS_ids.tsv')
    parser.add_argument('--fastqc_mg', help='metaG FastQC/MultiQC summary', required=True)
    parser.add_argument('--fastqc_mt', help='metaT FastQC/MultiQC summary', required=True)
    parser.add_argument('--tsv', help='output TSV file')
    args = parser.parse_args()

    # log
    setup_logger(logfile='{}.log'.format(args.tsv))
    logging.info('ARGS:\n{}'.format(args))

    # sample IDs
    samples = pandas.read_csv(args.ids, header=None).iloc[:,0]
    assert len(set(samples)) == 125, "{}: expected 125 unique sample IDs but got only {}".format(args.ids, len(set(samples)))

    # FastQC stats
    fastqc = {'mg': None, 'mt': None}
    fastqc['mg'] = pandas.read_csv(args.fastqc_mg, header=0, sep='\t', index_col='Sample', usecols=['Sample', 'Total Sequences'])
    fastqc['mt'] = pandas.read_csv(args.fastqc_mt, header=0, sep='\t', index_col='Sample', usecols=['Sample', 'Total Sequences'])
    # print(fastqc)

    # find FASTQ files
    fastq = []
    for pname in args.fastq_mg + args.fastq_mt + args.fastq_mg_comb + args.fastq_mt_comb:
        for fname in sorted(glob.glob("{}/*{}".format(pname, FASTQ_EXT))):
            try:
                dname = os.path.dirname(fname)
                sid = sampleIDfromFASTQ(fname)
                rid = readIDfromFASTQ(fname)
                if dname in args.fastq_mg + args.fastq_mg_comb:
                    fastq.append((sid, 'mg', rid, fname, 'combined' if dname in args.fastq_mg_comb else 'single'))
                elif dname in args.fastq_mt + args.fastq_mt_comb:
                    fastq.append((sid, 'mt', rid, fname, 'combined' if dname in args.fastq_mt_comb else 'single'))
            except Exception:
                logging.warning("SKIP FASTQ FILE: {}".format(fname))
                continue
    
    fastq = pandas.DataFrame(fastq, columns=['Sample_ID', 'Sequencing', 'Read_ID', 'FASTQ', 'Sequencing_Run'])
    fastq.set_index('FASTQ', drop=False, inplace=True, verify_integrity=True)
    fastq['Read_Count'] = fastq.apply(lambda x: get_read_count(x, fastqc), axis=1)

    # reshape: Sample_ID mg_R1 mg_R2 mt_R1 mt_R2
    samples = pandas.DataFrame(None, index=samples, columns=['mg_R1', 'mg_R2', 'mt_R1', 'mt_R2'])
    for gr_name, gr_df in fastq.groupby(['Sample_ID', 'Sequencing']):
        sid, seqtype = gr_name
        if sid not in samples.index:
            logging.warning('ADDED SAMPLE NOT INCLUDED IN GIVEN LIST: {}'.format(sid))
            samples.loc[sid,:] = numpy.nan
        if gr_df.shape[0] > 2:
            # if combined keep only those
            if sid in SAMPLES_COMB[seqtype]:
                gr_df = gr_df.loc[gr_df['Sequencing_Run'] == 'combined',:]
            # if mull. runs keep the one with most reads
            else:
                gr_df = gr_df.loc[get_sample_fastqs_with_most_reads(gr_df),]
        if gr_df.shape[0] == 2:
            assert is_paired(gr_df['FASTQ'].values), 'no paired FASTQ files: {}, {}'.format(gr_name, gr_df)
            assert pandas.isnull(samples.loc[sid, '{}_R1'.format(seqtype)]), 'R1 FASTQ file already set: {}, {}'.format(gr_name, gr_df)
            assert pandas.isnull(samples.loc[sid, '{}_R2'.format(seqtype)]), 'R2 FASTQ file already set: {}, {}'.format(gr_name, gr_df)
            for fq in gr_df.index:
                samples.loc[sid, '{}_{}'.format(seqtype, gr_df.loc[fq, 'Read_ID'])] = gr_df.loc[fq, 'FASTQ']
        else:
            raise Exception('More than two FASTQ files for {}: {}'.format(gr_name, gr_df))

    # print(samples)

    # check
    for sid in samples.index:
        for seqtype in SAMPLES_NA.keys():
            if all(pandas.isnull(samples.loc[sid, ['%s_R1' % seqtype, '%s_R2' % seqtype]])):
                logging.warning("NO FASTQ FILES: {}, {}".format(sid, seqtype))
                assert sid in SAMPLES_NA[seqtype] or not SAMPLES_COMPLETE[seqtype], "Sample {}, {} has no FASTQ files but is not among {}".format(sid, seqtype, SAMPLES_NA[seqtype])

    # save as TSV
    samples.to_csv(args.tsv, sep='\t', na_rep='NA', header=True, index=True, index_label='Sample_ID')
