# Create an FAA and protein sequence ID/contig ID mapping file to run PathoFact
#
# NOTE: RGI, included in PathoFact, can throw an error if there are any unexpected AA symbols, e.g. "*".
#       Need to remove stop codons denoted with "*" from the FASTA file and
#       skip any records containing "*" within their sequence.

import re
from Bio import SeqIO

def extract_id_from_gff_attrs(attr):
    """Extract feature ID from GFF attribute field string"""
    sid = None
    for a in attr.split(";"):
        if re.match("ID=", a):
            sid = re.sub("^ID=", "", a)
            break
    return sid

# logging
with open(snakemake.log[0], "w") as lfile:
    lfile.write("Input: faa={}, gff={}\nOutput: faa={}, tsv={}\n".format(
        snakemake.input.faa,
        snakemake.input.gff,
        snakemake.output.faa,
        snakemake.output.tsv,
    ))

# all record IDs
sids_all  = set()
# skipped record IDs
sids_skip = set()
# mapped (found in GFF) record IDs
sids_mapped = set()

# create FAA file
with open(snakemake.input.faa, "r") as ifile, open(snakemake.output.faa, "w") as ofile, open(snakemake.log[0], "a") as lfile:
    for record in SeqIO.parse(ifile, "fasta"):
        sids_all.add(record.id)
        # remove stop codon(s) at the end of the seq
        record.seq = record.seq.rstrip("*")
        # skip record if seq still contains a stop codon somewhere else
        if re.search("\*", str(record.seq)):
            sids_skip.add(record.id)
            lfile.write(f"skipping {record}\n")
            continue
        else:
            # remove description so record header has only seq ID (needed for PathoFact)
            record.description = ""
            # save
            SeqIO.write(record, ofile, "fasta")
    lfile.write("From {} sequences {} were skipped\n".format(len(sids_all), len(sids_skip)))

# create FAA/contig mapping file
with open(snakemake.input.gff, "r") as ifile, open(snakemake.output.tsv, "w") as ofile:
    for line in ifile:
        # http://www.ensembl.org/info/website/upload/gff.html
        cid, source, feature, start, end, score, strand, frame, attr = line.strip().split("\t")
        # seq ID
        sid = extract_id_from_gff_attrs(attr)
        assert sid is not None, f"No ID in {line}"
        assert sid not in sids_mapped, f"ID {line} was found multiple times in GFF"
        if sid in sids_all and sid not in sids_skip:
            sids_mapped.add(sid)
            ofile.write("{}\t{}\n".format(cid, sid))

# check if all IDs were found in GFF
assert sids_mapped == sids_all.difference(sids_skip), "Could not find all ids from FAA in GFF: {}".format(sids_all.difference(sids_skip).difference(sids_mapped))