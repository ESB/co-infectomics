#!/usr/bin/Rscript

## LOG FILE
sink(file=file(snakemake@log[[1]], open="wt"), type=c("output", "message"))

## IMPORT

# custom utils
source(snakemake@params$utils)

## INFO
print(sessionInfo())
print(snakemake@input)
print(snakemake@output)
print(snakemake@params)
print(snakemake@wildcards)

## DATA
# Metadata
mdata <- read_metadata(snakemake@input$metadata)
mdata <- factor_severity_in_metadata(mdata) # needed for plotting

## PLOTS
figs <- list()

# qPCR: counts
figs$qpcr_stool_counts <-
    ggplot(
        data=mdata,
        aes(
            x=qpcr__stool_result,
            fill=qpcr__stool_result
        )
    ) +
    geom_bar() +
    geom_text(stat='count', aes(label=..count..), vjust=-0.2) +
    scale_fill_manual(values=FIG_QPCR$colors, guide="none", na.value="gray") +
    facet_grid(cols=vars(cohort), scales="free_x", space="free") +
    labs(
        x="Consensus result",
        y="Number of samples",
        title="SARS-CoV-2 qPCR results",
        subtitle="Stool samples"
    ) +
    FIG_THEME_MAIN

for(sev_col in SEVERITY_COLS){
    mdata_sev <-
        mdata %>%
        filter(cohort == "SARS-CoV-2 positive") %>%
        mutate(Severity = get(sev_col))
    figs[[sprintf("qpcr_stool_counts_%s", sev_col)]] <-
        ggplot(
            data=mdata_sev,
            aes(
                x=qpcr__stool_result,
                fill=qpcr__stool_result
            )
        ) +
        geom_bar() +
        geom_text(stat='count', aes(label=..count..), vjust=-0.2) +
        scale_fill_manual(values=FIG_QPCR$colors, guide="none", na.value="gray") +
        facet_grid(cols=vars(Severity), scales="free_x", space="free") +
        labs(
            x="Consensus result",
            y="Number of samples",
            title="SARS-CoV-2 qPCR results",
            subtitle=sprintf("Stool samples, %s", sev_col)
        ) +
        FIG_THEME_MAIN
}

# TODO: normalized values

# PDF
pdf(snakemake@output$pdf, width=snakemake@params$width, height=snakemake@params$height)
for(fig in figs){ print(fig) }
dev.off()