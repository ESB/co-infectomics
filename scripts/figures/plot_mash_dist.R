#!/usr/bin/Rscript

## LOG FILE
sink(file=file(snakemake@log[[1]], open="wt"), type=c("output", "message"))

## IMPORT
suppressMessages(library(testit))
suppressMessages(library(dplyr))
suppressMessages(library(ggplot2))
suppressMessages(library(ggrepel))
suppressMessages(library(pheatmap))
suppressMessages(library(viridis))
suppressMessages(require(grid))

## FUNC
proc_annot_name <- function(x){
    return(sub("_R(1|2)$", "", basename(x)))
}

proc_mash_dist_name <- function(x){
    return(sub("_R(1|2).fastq.gz", "", basename(x)))
}

## INFO
print(sessionInfo())
print(snakemake@input)
print(snakemake@output)
print(snakemake@params)

## DATA
# read in
annot <- read.csv(file=snakemake@input$annot, sep="\t", header=TRUE, row.names=1, stringsAsFactors=FALSE, check.names=FALSE)
mdist <- read.csv(file=snakemake@input$mdist, sep="\t", header=TRUE, row.names=1, stringsAsFactors=FALSE, check.names=FALSE)
# parse names
annot <- annot[grepl('_R1$', rownames(annot)),]
rownames(annot) <- sapply(rownames(annot), proc_annot_name)
rownames(mdist) <- sapply(rownames(mdist), proc_mash_dist_name)
colnames(mdist) <- sapply(colnames(mdist), proc_mash_dist_name)
# checks
testit::assert(nrow(annot) == nrow(mdist))
testit::assert(all(sort(rownames(annot)) == sort(rownames(mdist))))
# re-order annot.s
annot <- annot[rownames(mdist),,drop=FALSE]

## PLOTS
figs <- list()

# hclust
for(ll in c("complete", "average")){
    htree <- hclust(d=as.dist(mdist), method=ll)
    figs[[sprintf('hclust_%s', ll)]] <- pheatmap(
        mdist,
        color=viridis(20),
        border_color="white",
        scale="none",
        cluster_rows=htree,
        cluster_cols=htree,
        annotation_col=annot[,c("Total Sequences"), drop=FALSE],
        annotation_colors=list(
            Total.Sequences=magma(20)
        ),
        fontsize=6,
        main=sprintf("Sample dissimilarity (Mash), %s linkage", ll),
        silent=TRUE
    )
}

# MDS
mds_2d <- cmdscale(d=as.dist(mdist), k=2, eig=FALSE, add=FALSE, x.ret=FALSE)
mds_2d <- data.frame(
    Sample_ID=rownames(mdist),
    Reads=annot[rownames(mdist),'Total Sequences',drop=TRUE],
    x=mds_2d[,1],
    y=mds_2d[,2],
    stringsAsFactors=FALSE
)
figs[['mds']] <-
    ggplot(data=mds_2d, aes(x=x, y=y)) +
    geom_point(size=4, shape=21, aes(fill=Reads), color='white') +
    geom_text_repel(
        aes(label=Sample_ID),
        box.padding=0.5,
        force_pull=2,
        max.overlaps=5, # Inf == plot all
        seed=42 # for reproducibility
    ) +
    scale_fill_viridis() +
    labs(
        x="",
        y="",
        title="MDS(mash dist)"
    ) +
    theme_minimal() +
    theme(
        panel.grid.major=element_blank(),
        panel.grid.minor=element_blank(),
        axis.text=element_blank(),
        axis.ticks=element_blank(),
        panel.border=element_rect(color="black", fill=NA)
    )


# PDF
pdf(snakemake@output[[1]], width=snakemake@params$width, height=snakemake@params$height)
for(fig_name in names(figs)){
    fig <- figs[[fig_name]]
    if(grepl('hclust_', fig_name)){
        grid.newpage()
        grid.draw(fig$gtable)
    } else {
        print(fig)
    }
}
dev.off()