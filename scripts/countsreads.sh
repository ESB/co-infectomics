#!/bin/bash

# Args:
#	1: path to folder with FASTQ files
#	2: path to output file

countreads() {
    r1=$1
    r2=$(echo "${r1}" | sed 's/_R1_/_R2_/')
    fname=$(basename -s ".fastq.gz" "${r1}" | sed 's/_S[0-9]\+_R[0-9]\+_[0-9]\+$//')
    # echo "${fname}, ${r1}, ${r2}"
    echo -e "${fname}\t$(( $(zcat ${r1} | wc -l) / 4 ))\t$(( $(zcat ${r2} | wc -l) / 4 ))"
}

export -f countreads

find "${1}" -type f -name "*_R1_*.fastq.gz" | sort | parallel countreads {} > "${2}"
