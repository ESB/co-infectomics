#!/bin/bash

# Combine multiple sequencing runs for samples
# which had lower sequencing depth in the 1st run than what was aimed for.

# source folder (where to take the source FASTQ files)
MT_SINGLE="/mnt/irisgpfs/projects/co-infectomics/raw/metat/fastq"
# target folder (where to save the combined FASTQ files)
MT_COMBINED="/mnt/irisgpfs/projects/co-infectomics/raw/metat_combined/fastq"

mkdir -p "${MT_COMBINED}"

# COINF_PRED0280
cat "${MT_SINGLE}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S3_R1_001.fastq.gz" "${MT_SINGLE}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S5_R1_001.fastq.gz" > "${MT_COMBINED}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S3S5_R1_001.fastq.gz"
cat "${MT_SINGLE}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S3_R2_001.fastq.gz" "${MT_SINGLE}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S5_R2_001.fastq.gz" > "${MT_COMBINED}/PRED0280_RNA_COINF_07052021_CMG_COINF_ESB_S3S5_R2_001.fastq.gz"

# COINF_PRED1167
cat "${MT_SINGLE}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S2_R1_001.fastq.gz" "${MT_SINGLE}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S6_R1_001.fastq.gz" > "${MT_COMBINED}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S2S6_R1_001.fastq.gz"
cat "${MT_SINGLE}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S2_R2_001.fastq.gz" "${MT_SINGLE}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S6_R2_001.fastq.gz" > "${MT_COMBINED}/PRED1167_RNA_COINF_07052021_CMG_COINF_ESB_S2S6_R2_001.fastq.gz"

# make the files write-protected
chmod -R a-w ${MT_COMBINED}
