#!/bin/bash

# Subset and split the sample table
# Example commands:
#   ./scripts/split_samples.sh 1 63 8 "tmp/samples."
#       get the 1st half of samples and split them into chunks of 8 samples per file saved as tmp/samples.*
#   ./scripts/split_samples.sh 64 126 8 "tmp/samples."
#       get the 2nd half of samples and split them into chunks of 8 samples per file saved as tmp/samples.*

# ARGS
#   1: start index (1..N; excluding the header, i.e. 1 == 1st sample, N == last sample)
#   2: end index (1..N; excluding the header, i.e. 1 == 1st sample, N == last sample)
#   3: chunk size, i.e. number of samples (lines) per chunk (not including the header)
#   4: output prefix
TAB="config/samples.tsv"
START=$1
END=$2
MAXEND=$(( $(cat ${TAB} | wc -l) - 1 ))
LINES=$3
PREFIX=$4

# checks
if (( ${START} < 1 )); then echo "Start index should be >= 1"; exit 1; fi
if (( ${END} > ${MAXEND} )); then echo "End index should be <= ${MAXEND}"; exit 1; fi
if (( ${START} > ${END} )); then echo "Start index should be <= end index"; exit 1; fi

# remove header | samples subset | split
tail -n +2 "${TAB}" | \
awk -F'\t' 'NR >= '${START}' && NR <= '${END}' {print $0}' | \
split -l ${LINES} --additional-suffix='.split.tsv' - "${PREFIX}"

# add header
find $(dirname ${PREFIX}) -type f -name "*.split.tsv" | while read fname; do
    sed -i "1 i\\$(head -n 1 ${TAB})" "${fname}"
done
