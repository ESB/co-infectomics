##################################################
# ASSUMPTIONS

assert all([RTYPES[mtype][0] == "raw" for mtype in MTYPES])
assert all([RTYPES[mtype][-1] == "preprocessed" for mtype in MTYPES])
assert RIDS[-1] == "se"

##################################################
# FastQC
# NOTE: cannot use IMP3's FastQC reports as the file names in the reports
#       are the same for all samples which is an issue for MultiQC.

# TODO: temp

# FastQC reports for intermediate IMP3 files (not raw/preprocessed)
rule fastqc:
    input:
        expand("qc/preproc/fastqc/{{mtype}}/{{rtype}}/{{sid}}.{rid}.fq.gz", rid=RIDS)
    output:
        temp(expand("qc/preproc/fastqc/{{mtype}}/{{rtype}}/{{sid}}.{rid}_fastqc.zip", rid=RIDS))
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
        rtype="|".join([rtype for rtype in RTYPES["mg"] + RTYPES["mt"] if rtype not in ["raw", "preprocessed"]]),
    threads:
        config["fastqc"]["threads"]
    conda:
        os.path.join(ENV_DIR, "fastqc.yaml")
    message:
        "FastQC: {input}"
    shell:
        "fastqc -q -f fastq -t {threads} --noextract --quiet -o $(dirname {output[0]}) {input}"

# FastQC reports for input FASTQ files
use rule fastqc as fastqc_raw with:
    input:
        expand("qc/preproc/fastqc/{{mtype}}/raw/{{sid}}.{rid}.fq.gz", rid=RIDS[:-1])
    output:
        temp(expand("qc/preproc/fastqc/{{mtype}}/raw/{{sid}}.{rid}_fastqc.zip", rid=RIDS[:-1]))
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),

# FastQC reports for final FASTQ files
use rule fastqc as fastqc_proc with:
    input:
        expand("qc/preproc/fastqc/{{mtype}}/preprocessed/{{sid}}.{rid}.fq", rid=RIDS)
    output:
        temp(expand("qc/preproc/fastqc/{{mtype}}/preprocessed/{{sid}}.{rid}_fastqc.zip", rid=RIDS))
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),

# FastQC summary report
rule multiqc_fastqc:
    input:
        lambda wildcards: expand(
            "qc/preproc/fastqc/{{mtype}}/{{rtype}}/{sid}.{rid}_fastqc.zip",
            sid=get_samples_with_omic(wildcards.mtype),
            rid=RIDS if wildcards.rtype != "raw" else RIDS[:-1],
        )
    output:
        ifiles="qc/preproc/fastqc/{mtype}/{rtype}/multiqc_input.txt",
        html="qc/preproc/fastqc/{mtype}/{rtype}/multiqc_report.html",
        stat_fqc="qc/preproc/fastqc/{mtype}/{rtype}/multiqc_data/multiqc_fastqc.txt",
    log:
        "qc/preproc/fastqc/{mtype}/{rtype}/multiqc.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        rtype="|".join(RTYPES["mg"] + RTYPES["mt"]),
    conda:
        os.path.join(ENV_DIR, "multiqc.yaml")
    message:
        "MultiQC: {wildcards.mtype}"
    shell:
        "echo \'{input}\' | sed 's/\s\+/\\n/g' > {output.ifiles} && "
        # -p: export plots
        # -f: overwrite existing report
        # -m fastqc: use only module for FastQC
        # --file-list: file w/ input files to be used
        "multiqc --interactive -p -f -m fastqc --file-list {output.ifiles} -o $(dirname {output.html}) &> {log}"

# Number of reads summary
localrules: multiqc_reads
rule multiqc_reads:
    input:
        lambda wildcards: expand(
            "qc/preproc/fastqc/{{mtype}}/{rtype}/multiqc_data/multiqc_fastqc.txt",
            rtype=RTYPES[wildcards.mtype]
        )
    output:
        "qc/preproc/fastqc/{mtype}/summary.reads.tsv"
    run:
        summary = []
        for fname in input:
            fname_2 = os.path.dirname(os.path.dirname(fname))
            fname_mtype = os.path.basename(os.path.dirname(fname_2))
            fname_rtype = os.path.basename(fname_2)
            # FastQC stats
            df = pandas.read_csv(fname, sep="\t", header=0, usecols=["Sample", "Total Sequences"])
            df = df.loc[df.Sample.str.contains("\.(r1|se)$"),] # keep only R1 and SE
            df = df.assign(
                Read_ID=df["Sample"].apply(lambda x: 'SE' if re.search('\.se$', x) else 'PE'), # if PE or SE reads
                Sequencing=fname_mtype,
                Step=fname_rtype,
            )
            df["Sample"] = df["Sample"].apply(lambda x: re.sub('\.(r1|se)$', '', x)) # rm suffixes from sample IDs
            df.rename(columns={"Total Sequences": "Read_count", "Sample": "Sample_ID"}, inplace=True, errors='raise') # rename columns
            df.set_index(["Sample_ID", "Sequencing", "Step", "Read_ID"], drop=True, inplace=True, verify_integrity=True) # set multi-index
            # save
            summary.append(df)
        summary = pandas.concat(summary)
        summary.to_csv(output[0], sep='\t', na_rep='NA', header=True, index=True)

##################################################
# PhiX/human

# check for the presence of PhiX/human reads in processed FASTQ files
# https://github.com/DerrickWood/kraken2/wiki/Manual
rule kraken2_phix_human:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.preprocessed.fq" % RUNID,
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.preprocessed.fq" % RUNID,
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.preprocessed.fq" % RUNID,
        db=KRAKEN2_DB_PH_FILES,
    output:
        report_pe=temp("qc/preproc/kraken2/{mtype}/{sid}.pe.phix_human.report.txt"),
        report_se=temp("qc/preproc/kraken2/{mtype}/{sid}.se.phix_human.report.txt"),
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads:
        config["kraken2"]["threads"]
    params:
        params="--use-names"
    conda:
        os.path.join(ENV_DIR, "kraken2.yaml")
    message:
        "Check: kraken2, human/PhiX DB: {wildcards.sid}, {wildcards.mtype}"
    shell:
        "kraken2 --db $(dirname {input.db}) --report {output.report_pe} --threads {threads} {params.params} --paired {input.r1} {input.r2} > /dev/null && "
        "kraken2 --db $(dirname {input.db}) --report {output.report_se} --threads {threads} {params.params} {input.se} > /dev/null"

localrules: kraken2_phix_human_summary
rule kraken2_phix_human_summary:
    input:
        lambda wildcards: expand(
            "qc/preproc/kraken2/{{mtype}}/{sid}.{rid}.phix_human.report.txt",
            mtype=mtype,
            sid=get_samples_with_omic(wildcards.mtype),
            rid=RIDS_2,
        )
    output:
        "qc/preproc/kraken2/{mtype}/summary.phix_human.tsv"
    run:
        from scripts.utils import read_kraken2_report
        summary = []
        for fname in input:
            fname_mtype = os.path.basename(os.path.dirname(fname))
            fname_sid = os.path.basename(fname).split(".")[0]
            fname_rid = os.path.basename(fname).split(".")[1]
            # kraken2 report
            df = read_kraken2_report(fname)
            df = df.loc[df["tax_id"].apply(lambda x: x in {0, 9606, 10847}),["count_direct", "tax_name"]] # extract relevant taxa: unclass, human, phix
            df.rename(columns={"count_direct": "Read_count", "tax_name": "Taxon"}, inplace=True, errors="raise") # rename columns
            df = df.assign(Sample_ID=fname_sid, Sequencing=fname_mtype, Read_ID=fname_rid)
            df.set_index(["Sample_ID", "Sequencing", "Read_ID", "Taxon"], drop=True, inplace=True, verify_integrity=True) # set multi-index
            # save
            summary.append(df)
        summary = pandas.concat(summary)
        summary.to_csv(output[0], sep="\t", na_rep="NA", header=True, index=True)

##################################################
# Sample similarity

# TODO: https://github.com/marbl/Mash/issues/164
rule mash_sketch:
    input: 
        fq="qc/preproc/concat/{mtype}/preprocessed/{sid}.fq",
    output:
        temp("qc/preproc/mash/{mtype}/{sid}.msh")
    log:
        "qc/preproc/mash/{mtype}/{sid}.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads:
        1 # cannot use more than 1 if sketching reads (-r)
    params:
        # https://mash.readthedocs.io/en/latest/sketches.html#sketching-read-sets
        # -r: input is a read set
        # -m: if > 1, ignore unique k-mers
        # -k: 31, more specific than 21 (default)
        # -s: sketch size, 100 * default since metagenome > genome
        # -S: seed
        params="-r -m 3 -s 100000 -k 31 -S 42",
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Mash: sketch reads: {wildcards.sid}, {wildcards.mtype}"
    shell:
        "ofile={output} && mash sketch -o ${{ofile%.*}} {params.params} {input} &> {log}"

rule mash_paste:
    input:
        lambda wildcards: expand(
            "qc/preproc/mash/{{mtype}}/{sid}.msh",
            sid=get_samples_with_omic(wildcards.mtype),
        )
    output:
        "qc/preproc/mash/{mtype}/all.msh"
    wildcard_constraints:
        mtype="|".join(MTYPES),
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Mash: paste read sketches: {wildcards.mtype}"
    shell:
        "ofile={output} && mash paste ${{ofile%.*}} {input}"

rule mash_dist:
    input:
        "qc/preproc/mash/{mtype}/all.msh"
    output:
        "qc/preproc/mash/{mtype}/all.dist"
    wildcard_constraints:
        mtype="|".join(MTYPES),
    threads:
        config["mash"]["threads"]
    conda:
        os.path.join(ENV_DIR, "mash.yaml")
    message:
        "Mash: sample distance from read sketches: {wildcards.mtype}"
    shell:
        "mash dist -p {threads} -t {input} {input} > {output}"

localrules: mash_plot_annot
rule mash_plot_annot:
    input:
        "qc/preproc/fastqc/{mtype}/summary.reads.tsv"
    output:
        temp("qc/preproc/mash/{mtype}/summary.reads.tsv")
    run:
        # reformat the table:
        #   keep only stat.s from processed reads
        #   combine read counts for PE and SE
        df = pandas.read_csv(input[0], header=0, sep="\t")
        assert "preprocessed" in df.Step.values
        df = df.loc[df.Step == "preprocessed",:]
        counts = pandas.DataFrame(index=set(df.Sample_ID), columns=["Read_count"])
        for gr_id, gr_df in df.groupby(["Sample_ID"]):
            assert gr_df.shape[0] == 2
            assert set(gr_df.Read_ID) == {"PE", "SE"}
            counts.loc[gr_id, "Read_count"] = gr_df.Read_count.sum()
        counts.to_csv(output[0], sep="\t", header=True, index=True, index_label="Sample_ID")

rule mash_plot:
    input:
        mdist="qc/preproc/mash/{mtype}/all.dist",
        annot="qc/preproc/mash/{mtype}/summary.reads.tsv",
    output:
        "qc/preproc/mash/{mtype}/all.pdf"
    log:
        "qc/preproc/mash/{mtype}/all.pdf.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
    threads:
        1
    params:
        height=12,
        width=12
    conda:
        os.path.join(ENV_DIR, "r.yaml")
    message:
        "Mash: distance plot: {input}"
    script:
        os.path.join(SRC_DIR, "figures", "plot_mash_dist_v2.R")
