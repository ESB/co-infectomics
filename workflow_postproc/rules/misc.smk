# https://nonpareil.readthedocs.io/en/latest/redundancy.html#output
# https://nonpareil.readthedocs.io/en/latest/curves.html
rule join_nonpareil:
    input:
        expand("preproc/mg/{sid}/{runid}/Stats/mg/mg.{rid}_nonpareil.diversity.txt", sid=SIDS_MG, rid=["r1", "r2"], runid=config["imp3"]["assembly"]["run"])
    output:
        div="preproc/mg/nonpareil.mg.diversity.tsv"
    message:
        "Join nonpareil output: {output}"
    run:
        div = pandas.DataFrame(None, index=range(len(input)), columns=["Sample_ID", "Read_ID", "Diversity"])
        i = 0 # current row to save the data
        for fname in input:
            sid = os.path.normpath(fname).split(os.path.sep)[2] # sample ID
            rid = re.match("mg\.(?P<rid>r1|r2)_nonpareil\.diversity\.txt", os.path.basename(fname)).group("rid") # read ID
            sid_div = pandas.read_csv(fname, header=None, sep="\t", index_col=0, names=["value"])
            div.iloc[i,] = [sid, rid, sid_div.loc["nopareil_diversity","value"]]
            i += 1
        # mean/min/max
        div_grouped = pandas.DataFrame(None, index=sorted(set(div["Sample_ID"])), columns=["Diversity_Mean", "Diversity_Min", "Diversity_Max"])
        for gr_name, gr_df in div.groupby(["Sample_ID"]):
            div_grouped.loc[gr_name,:] = [gr_df.Diversity.mean(), gr_df.Diversity.min(), gr_df.Diversity.max()]
        # save
        div_grouped.to_csv(output.div, sep="\t", header=True, index=True, index_label="Sample_ID")