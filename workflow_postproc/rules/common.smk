##################################################
# Functions

def get_samples_with_omic(omic):
    """Get samples which have omic data"""
    assert omic in ["mt", "mg"]
    return SAMPLES.loc[(pandas.notnull(SAMPLES["%s_R1" % omic])) & (pandas.notnull(SAMPLES["%s_R2" % omic]))].index
