##################################################
# Fastv
# https://github.com/OpenGene/fastv

# check for SARS-CoV-2 in processed FASTQ files
rule fastv_sars:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        kmer=SARS_FASTA_KMER,
        genomes=SARS_FASTA_GENOMES,
    output:
        json_pe="qc/preproc/fastv/{mtype}/{sid}.pe.sars-cov-2.json",
        html_pe="qc/preproc/fastv/{mtype}/{sid}.pe.sars-cov-2.html",
        json_se="qc/preproc/fastv/{mtype}/{sid}.se.sars-cov-2.json",
        html_se="qc/preproc/fastv/{mtype}/{sid}.se.sars-cov-2.html",
    log:
        "qc/preproc/fastv/{mtype}/{sid}.sars-cov-2.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads:
        config["fastv"]["threads"]
    params:
        # report title
        title=lambda wildcards: "SARS-CoV-2 report for {}, {}".format(wildcards.sid, wildcards.mtype),
        # disable processing, do not overwrite, enable log info
        params="--disable_adapter_trimming --disable_trim_poly_g --disable_quality_filtering --disable_length_filtering --dont_overwrite --verbose",
    conda:
        os.path.join(ENV_DIR, "fastv.yaml")
    message:
        "fastv (SARS-CoV-2): {wildcards.sid}"
    shell:
        # PE
        "fastv --in1 {input.r1} --in2 {input.r2} --kmer {input.kmer} --genomes {input.genomes} "
        "--json {output.json_pe} --html {output.html_pe} --report_title \'{params.title}, PE\' {params.params} --thread {threads} &>  {log} && "
        # SE
        "fastv -i {input.se} --kmer {input.kmer} --genomes {input.genomes} "
        "--json {output.json_se} --html {output.html_se} --report_title \'{params.title}, SE\' {params.params} --thread {threads} &>> {log}"

# check for viruses in processed FASTQ files
rule fastv_viruses:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        kmerc=VIR_FASTA_KMERC,
    output:
        json_pe="qc/preproc/fastv/{mtype}/{sid}.pe.viruses.json",
        html_pe="qc/preproc/fastv/{mtype}/{sid}.pe.viruses.html",
        json_se="qc/preproc/fastv/{mtype}/{sid}.se.viruses.json",
        html_se="qc/preproc/fastv/{mtype}/{sid}.se.viruses.html",
    log:
        "qc/preproc/fastv/{mtype}/{sid}.viruses.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads:
        config["fastv"]["threads"]
    params:
        # report title
        title=lambda wildcards: "Viruses report for {}, {}".format(wildcards.sid, wildcards.mtype),
        # disable processing, do not overwrite, enable log info
        params="--disable_adapter_trimming --disable_trim_poly_g --disable_quality_filtering --disable_length_filtering --dont_overwrite --verbose",
    conda:
        os.path.join(ENV_DIR, "fastv.yaml")
    message:
        "fastv (viruses): {wildcards.sid}"
    shell:
        # https://github.com/OpenGene/fastv#options
        "fastv --in1 {input.r1} --in2 {input.r2} --kmer_collection {input.kmerc} "
        "--json {output.json_pe} --html {output.html_pe} --report_title \'{params.title}, PE\' {params.params} --thread {threads} &>  {log} && "
        "fastv -i {input.se} --kmer_collection {input.kmerc} "
        "--json {output.json_se} --html {output.html_se} --report_title \'{params.title}, SE\' {params.params} --thread {threads} &>> {log}"

localrules: fastv_viruses_summary
rule fastv_viruses_summary:
    input:
        lambda wildcards: expand(
            "qc/preproc/fastv/{{mtype}}/{sid}.{rid}.viruses.json",
            sid=get_samples_with_omic(wildcards.mtype),
            rid=RIDS_2,
        )
    output:
        "qc/preproc/fastv/{mtype}/summary.viruses.tsv"
    message:
        "fastv summary (viruses): {output}"
    run:
        import json
        summary = []
        for fname in input:
            fname_sid = os.path.basename(fname).split(".")[0]
            fname_rid = os.path.basename(fname).split(".")[1]
            fname_mtype = os.path.basename(os.path.dirname(fname))
            # stats
            df = None
            with open(fname) as ifile:
                df = pandas.DataFrame(json.load(ifile)["kmer_collection_scan_result"]).transpose() # table: stats x genomes
            df = df.assign(
                Sequencing=fname_mtype,
                Sample_ID=fname_sid,
                Read_ID=fname_rid,
                Genome=df.index,
            )
            df.set_index(["Sample_ID", "Sequencing", "Read_ID", "Genome"], drop=True, inplace=True, verify_integrity=True) # set multi-index
            # save
            summary.append(df)
        summary = pandas.concat(summary)
        summary.to_csv(output[0], sep="\t", na_rep="NA", header=True, index=True)

localrules: fastv_sars_summary
rule fastv_sars_summary:
    input:
        lambda wildcards: expand(
            "qc/preproc/fastv/{{mtype}}/{sid}.{rid}.sars-cov-2.json",
            sid=get_samples_with_omic(wildcards.mtype),
            rid=RIDS_2,
        )
    output:
        "qc/preproc/fastv/{mtype}/summary.sars-cov-2.tsv"
    message:
        "fastv summary (SARS-CoV-2): {output}"
    run:
        import json
        summary = []
        for fname in input:
            fname_sid = os.path.basename(fname).split(".")[0]
            fname_rid = os.path.basename(fname).split(".")[1]
            fname_mtype = os.path.basename(os.path.dirname(fname))
            with open(fname) as ifile:
                fname_json = json.load(ifile)
                summary.append({
                    "Sequencing": fname_mtype,
                    "Sample_ID": fname_sid,
                    "Read_ID": fname_rid,
                    "Result": fname_json["kmer_detection_result"]["result"],
                    "Mean_coverage": fname_json["kmer_detection_result"]["mean_coverage"],
                })
        summary = pandas.DataFrame(summary)
        summary.to_csv(output[0], sep="\t", na_rep="NA", header=True, index=False, index_label=False)

##################################################
# PACIFIC
# https://github.com/pacific-2020/pacific/

rule pacific:
    input:
        fq="qc/preproc/concat/{mtype}/preprocessed/{sid}.fq",
        model=PACIFIC_MODEL,
        # ignore the timestamp since they are part of the submodul repo
        tokenizer=ancient(os.path.join(MOD_DIR, "pacific", "model", "tokenizer.01.pacific_9mers.pickle")),
        label_maker=ancient(os.path.join(MOD_DIR, "pacific", "model", "label_maker.01.pacific_9mers.pickle")),
    output:
        fq=temp("qc/preproc/pacific/{mtype}/pacificoutput_{sid}.fq.gz"),
        summary="qc/preproc/pacific/{mtype}/{sid}.txt",
        summary_tmp=temp("qc/preproc/pacific/{mtype}/{sid}.fq_summary.txt"),
    log:
        "qc/preproc/pacific/{mtype}/{sid}.log"
    wildcard_constraints:
        # mtype="|".join(MTYPES),
        mtype="mt",
        sid="|".join(SAMPLES.index),
    threads:
        config["pacific"]["threads"]
    params:
        exe=os.path.join(MOD_DIR, "pacific", "PACIFIC.py"),
    conda:
        os.path.join(ENV_DIR, "pacific.yaml")
    message:
        "fastv (SARS-CoV-2): {wildcards.sid}"
    shell:
        "python {params.exe} -i {input.fq} -m {input.model} -t {input.tokenizer} -l {input.label_maker} -o $(dirname {output.summary}) -f fastq --chunk_size 1000000 &> {log} && "
        "rsync -tP {output.summary_tmp} {output.summary} &>> {log}"

##################################################
# bbtools
# https://jgi.doe.gov/data-and-tools/bbtools

rule bbmap_sars:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.preprocessed.fq" % config["imp3"]["preprocessing"]["run"],
        ref=SARS_FASTA_GENOMES,
    output:
        # reads matching the ref.s
        r1="sars-cov-2/{mtype}/{sid}.r1.fq.gz",
        r2="sars-cov-2/{mtype}/{sid}.r2.fq.gz",
        se="sars-cov-2/{mtype}/{sid}.se.fq.gz",
        # stats
        stats_pe="sars-cov-2/{mtype}/{sid}.pe.stats.txt",
        stats_se="sars-cov-2/{mtype}/{sid}.se.stats.txt",
        cov_pe="sars-cov-2/{mtype}/{sid}.pe.cov.txt",
        cov_se="sars-cov-2/{mtype}/{sid}.se.cov.txt",
    log:
        "sars-cov-2/{mtype}/{sid}.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads:
        config["bbtools"]["sars"]["threads"]
    params:
        # from manual: ca. 85% of the machine’s physical memory
        mem="-Xmx%dg" % round(config["bbtools"]["sars"]["mem"] * config["bbtools"]["sars"]["threads"] * 0.85),
        # https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/bbmap-guide/
        # don't save ref index; in/out quality = auto-detection; zip output; keep read order; print unmapped
        # idfilter Independant of minid; sets exact minimum identity allowed for alignments to be printed. Range 0 to 1.
        params="nodisk=t qin=auto qout=auto ziplevel=9 ordered=t printunmappedcount=t idfilter=0.95",
    conda:
        os.path.join(ENV_DIR, "bbtools.yaml")
    message:
        "bbduk (SARS-CoV-2): {wildcards.sid}"
    shell:
        # PE
        "bbmap.sh ref={input.ref} in1={input.r1} in2={input.r2} outm1={output.r1} outm2={output.r2} statsfile={output.stats_pe} covstats={output.cov_pe} "
        "threads={threads} {params.mem} {params.params} &> {log} && "
        # SE
        "bbmap.sh ref={input.ref} in={input.se} outm={output.se} statsfile={output.stats_se} covstats={output.cov_se} "
        "threads={threads} {params.mem} {params.params} &>> {log}"

rule bbmap_sars_fastq_stats:
rule count_sars_reads:
    input:
        lambda wildcards: expand(
            "sars-cov-2/{mtype}/{sid}.{rid}.fq.gz",
            mtype=wildcards.mtype,
            rid=RIDS,
            sid=get_samples_with_omic(wildcards.mtype),
        )
    output:
        "sars-cov-2/{mtype}/fastq.stats.tsv"
    log:
        "sars-cov-2/{mtype}/fastq.stats.log"
    wildcard_constraints:
        mtype="|".join(MTYPES)
    conda:
        os.path.join(ENV_DIR, "seqkit.yaml")
    message:
        "SARS-CoV-2 reads stats {wildcards.mtype}"
    shell:
        "seqkit stats {input} -T > {output} 2> {log}"