##################################################
# Data(bases)

# fastv, SARS-CoV-2
rule download_fastv_sars:
    output:
        fasta=os.path.join(DBS_DIR, "fastv", "SARS-CoV-2.{ftype}.fa"),
        md5=os.path.join(DBS_DIR, "fastv", "SARS-CoV-2.{ftype}.md5"),
    log:
        os.path.join(DBS_DIR, "fastv", "SARS-CoV-2.{ftype}.log")
    params:
        url=lambda wildcards: config["fastv"]["sars"][wildcards.ftype]
    message:
        "Download SARS-CoV-2 genome for fastv: {output.fasta}"
    shell:
        "wget -O {output.fasta} {params.url} -o {log} && "
        "cd $(dirname {output.fasta}) && md5sum $(basename {output.fasta}) > $(basename {output.md5})"

# fastv, viruses
rule download_fastv_viruses:
    output:
        fasta=VIR_FASTA_KMERC,
        md5="%s.md5" % os.path.splitext(VIR_FASTA_KMERC)[0],
    log:
        "%s.log" % os.path.splitext(VIR_FASTA_KMERC)[0]
    params:
        url=config["fastv"]["viruses"]["kmerc"]
    message:
        "Download SARS-CoV-2 genome for fastv: {output.fasta}"
    shell:
        "wget -O {output.fasta}.gz {params.url} -o {log} && "
        "gunzip {output.fasta}.gz && "
        "cd $(dirname {output.fasta}) && md5sum $(basename {output.fasta}) > $(basename {output.md5})"

# pacific model
rule download_pacific_model:
    output:
        model=PACIFIC_MODEL,
        md5="%s.md5" % os.path.splitext(PACIFIC_MODEL)[0],
    log:
        "%s.log" % os.path.splitext(PACIFIC_MODEL)[0],
    params:
        url=config["pacific"]["model"]
    message:
        "Download PACIFIC model: {output.model}"
    shell:
        "wget -O {output.model} {params.url} -o {log} && "
        "cd $(dirname {output.model}) && md5sum $(basename {output.model}) > $(basename {output.md5})"

# kraken2 PhiX/human
rule download_kraken2db_phix_human:
    output:
        files=KRAKEN2_DB_PH_FILES,
        md5="%s.md5" % KRAKEN2_DB_PH,
    log:
        "%s.log" % KRAKEN2_DB_PH,
    params:
        url=config["kraken2"]["humanphix"],
        odir=KRAKEN2_DB_PH,
    message:
        "Download reference FASTA: {output}"
    shell:
        "wget -O {params.odir}.tar.gz {params.url} -o {log} && "
        "cd $(dirname {params.odir}.tar.gz) && "
        "tar -xzvf $(basename {params.odir}.tar.gz) &>> {log} && "
        "md5sum $(basename {params.odir})/*.k2d && > {output.md5} && "
        "rm $(basename {params.odir}.tar.gz)"

##################################################
# FASTQ files

localrules: fastqc_input_raw, fastqc_input

# Symlink to raw/input FASTQ files
rule fastqc_input_raw:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.fq.gz" % RUNID,
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.fq.gz" % RUNID,
    output:
        r1=temp("qc/preproc/fastqc/{mtype}/raw/{sid}.r1.fq.gz"),
        r2=temp("qc/preproc/fastqc/{mtype}/raw/{sid}.r2.fq.gz"),
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    shell:
        "ln -svf $(realpath {input.r1}) {output.r1} && "
        "ln -svf $(realpath {input.r2}) {output.r2}"

# SYmlink to processed FASTQ files
rule fastqc_input:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.{rtype}.{ext}" % RUNID,
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.{rtype}.{ext}" % RUNID,
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.{rtype}.{ext}" % RUNID,
    output:
        r1=temp("qc/preproc/fastqc/{mtype}/{rtype}/{sid}.r1.{ext}"),
        r2=temp("qc/preproc/fastqc/{mtype}/{rtype}/{sid}.r2.{ext}"),
        se=temp("qc/preproc/fastqc/{mtype}/{rtype}/{sid}.se.{ext}"),
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
        rtype="|".join(RTYPES['mg'][1:] + RTYPES['mt'][1:]), # w/o 'raw'
        ext="fq(\.gz)?",
    shell:
        "ln -svf $(realpath {input.r1}) {output.r1} && "
        "ln -svf $(realpath {input.r2}) {output.r2} && "
        "ln -svf $(realpath {input.se}) {output.se}"

# Concat FASTQ files (not for raw/input FASTQ files)
rule concat_fastq:
    input:
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.{rtype}.{ext}" % RUNID,
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.{rtype}.{ext}" % RUNID,
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.{rtype}.{ext}" % RUNID,
    output:
        temp("qc/preproc/concat/{mtype}/{rtype}/{sid}.{ext}"),
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
        rtype="|".join(set(RTYPES['mg'] + RTYPES['mt']).difference(["raw"])),
        ext="fq(\.gz)?",
    shell:
        "cat {input} > {output}"
