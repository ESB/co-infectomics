# NOTE Postprocessing workflow, to be run after the preprocessing workflow

##################################################
# INIT

include:
    "rules/init.smk"

include:
    "rules/common.smk"

# multi-omics data type
MTYPES = []
if any((pandas.notnull(SAMPLES['mg_R1'])) & (pandas.notnull(SAMPLES['mg_R2']))):
    MTYPES += ["mg"]
if any((pandas.notnull(SAMPLES['mt_R1'])) & (pandas.notnull(SAMPLES['mt_R2']))):
    MTYPES += ["mt"]

# read IDs for FASTQ files
RIDS   = ["r1", "r2", "se"]
RIDS_2 = ["pe", "se"]

# Sample IDs w/ MG/MT data
SIDS_MG = SAMPLES.loc[(pandas.notnull(SAMPLES.mg_R1)) & (pandas.notnull(SAMPLES.mg_R2))].index
SIDS_MT = SAMPLES.loc[(pandas.notnull(SAMPLES.mt_R1)) & (pandas.notnull(SAMPLES.mt_R2))].index
SIDS_MGMT = SAMPLES.loc[
    (pandas.notnull(SAMPLES.mg_R1)) &
    (pandas.notnull(SAMPLES.mg_R2)) &
    (pandas.notnull(SAMPLES.mt_R1)) &
    (pandas.notnull(SAMPLES.mt_R2))
].index

# raw/processed FASTQ files: 'raw' = input FASTQ files, other = IMP3 preprocessing output (diff. steps)
RTYPES = {
    'mg': ['raw', 'trimmed', 'preprocessed'],
    'mt': ['raw', 'trimmed', 'preprocessed'],
}

# IMP3 preprocessing run ID
RUNID = config["imp3"]["preprocessing"]["run"]

# fastv
SARS_FASTA_KMER    = os.path.join(DBS_DIR, "fastv", "SARS-CoV-2.kmer.fa")
SARS_FASTA_GENOMES = os.path.join(DBS_DIR, "fastv", "SARS-CoV-2.genomes.fa")
VIR_FASTA_KMERC    = os.path.join(DBS_DIR, "fastv", "viral.kc.fasta")

# pacific
PACIFIC_MODEL = os.path.join(DBS_DIR, "pacific", "pacific.h5")

# kraken2
KRAKEN2_DB_PH = os.path.join(DBS_DIR, "kraken2/kraken2_human_and_phiX_db")
KRAKEN2_DB_PH_FILES = expand(os.path.join(KRAKEN2_DB_PH, "{fname}"), fname=["hash.k2d", "opts.k2d", "taxo.k2d"])

##################################################
# TARGETS
# (targets which cannot be easily specified in the main rule itself)

TARGETS_FASTQC_MULTIQC = []
for mtype in MTYPES:
    TARGETS_FASTQC_MULTIQC += expand(
        "qc/preproc/fastqc/{mtype}/{rtype}/multiqc_report.html",
        mtype=mtype, rtype=RTYPES[mtype]
    )

# MT-only targets
TARGETS_PACIFIC = []
TARGETS_BBTOOLS_SARS = []
for mtype in ['mt']:
    TARGETS_PACIFIC += expand(
        "qc/preproc/pacific/{mtype}/{sid}.txt",
        mtype=mtype,
        sid=get_samples_with_omic(mtype),
    )
    TARGETS_BBTOOLS_SARS += expand(
        "sars-cov-2/{mtype}/{sid}.{rid}.fq.gz",
        mtype=mtype,
        rid=RIDS,
        sid=get_samples_with_omic(mtype),
    )

##################################################
# RULES

include:
    "rules/input.smk"

include:
    "rules/qc.smk"

include:
    "rules/viruses.smk"

include:
    "rules/misc.smk"

# "Master" rule
localrules: all
rule all:
    input:
        # FastQC/MultiQC
        TARGETS_FASTQC_MULTIQC,
        "sars-cov-2/mt/fastq.stats.tsv",
        expand(
            "qc/preproc/fastqc/{mtype}/summary.reads.tsv",
            mtype=MTYPES
        ),
        # Kraken2
        expand(
            "qc/preproc/kraken2/{mtype}/summary.phix_human.tsv",
            mtype=MTYPES
        ),
        # fastv
        expand(
            "qc/preproc/fastv/{mtype}/summary.{fastv}.tsv",
            mtype=MTYPES, fastv=['sars-cov-2', 'viruses'],
        ),
        # # pacific
        # TARGETS_PACIFIC,
        # mash
        expand(
            "qc/preproc/mash/{mtype}/all.pdf",
            mtype=MTYPES
        ),
        # bbtools (SARS-CoV-2)
        TARGETS_BBTOOLS_SARS,
        # diversity from nonpareil
        "preproc/mg/nonpareil.mg.diversity.tsv",
