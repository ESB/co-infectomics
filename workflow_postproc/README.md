# About

Workflow for postprocessing of preprocessed metaG/metaT sequencing data: quality control (QC) steps, virus detection and summaries from the preprocessing step.

Main steps:
- QC: `MultiQC` summary from `FastQC` reports for raw and processed FASTQ files
- QC: PhiX and human contamination check using `kraken2`
- QC: sample (dis)similarity estimation using `mash`
- SARS-CoV-2 detection using `fastv`
- Virus detection using `fastv`
- ~~SARS-CoV-2/virus detection using `pacific`~~
  - *currently not enabled/used*
- Diversity summary from `nonpareil` output

## Configuration and execution

**When to run:**
> To be executed after the preprocessing workflow.

Config files:
- `config/config.yaml`: main config file for all workflows
- `workflow_postproc/profiles/iris_slurm/slurm.yaml`: `slurm` config
- `workflow_postproc/profiles/iris_slurm/config.yaml`: `snakemake` parameters

Before executing the workflow, check **all** config files listed above, especially lines tagged with `USER_INPUT`.

It is **not** recommended to run the workflow on the access node:
though all computation-intensive steps should be submitted via `slurm`, it is better to avoid doing that especially for (very) long jobs.

```bash
# start an interactive session

# activate the main conda env.
conda activate coinf

# dry-run
snakemake --profile workflow_postproc/profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile workflow_postproc/profiles/iris_slurm
```