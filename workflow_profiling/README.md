# About

Workflow for profiling-based analysis of the processed metaG/metaT sequencing data.

Main steps:
- setup and run `metaphlan3` (tax. profiling)
- setup and run `humann3` (func. profiling)
- setup and run `rgi` (AMR profiling)

## Configuration and execution

**When to run:**
> To be executed after the preprocessing workflow.

Config files:
- `config/config.yaml`: main config file for all workflows
- `workflow_profiling/profiles/iris_slurm/slurm.yaml`: `slurm` config
- `workflow_profiling/profiles/iris_slurm/config.yaml`: `snakemake` parameters

Before executing the workflow, check **all** config files listed above, especially lines tagged with `USER_INPUT`.

It is **not** recommended to run the workflow on the access node:
though all computation-intensive steps should be submitted via `slurm`, it is better to avoid doing that especially for (very) long jobs.

```bash
# start an interactive session

# activate the main conda env.
conda activate coinf

# dry-run
snakemake --profile workflow_profiling/profiles/iris_slurm --dry-run
# execute w/ slurm
snakemake --profile workflow_profiling/profiles/iris_slurm
```