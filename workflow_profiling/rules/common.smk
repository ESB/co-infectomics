# Functions for snakemake rules

##################################################
# The Resistance Gene Identifier (RGI)

def rgi_get_data_download_target():
    """For snakemake: get path for RGI data download"""
    file_name = os.path.basename(config["rgi"]["db"]["data"]["url"])
    return os.path.join(DBS_DIR, "rgi", "data", file_name)

def rgi_get_variants_download_target():
    """For snakemake: get path for RGI variants data download"""
    file_name = os.path.basename(config["rgi"]["db"]["variants"]["url"])
    return os.path.join(DBS_DIR, "rgi", "variants", file_name)

def rgi_get_extracted_data_target(file_name):
    """For snakemake: get file path for an RGI data file extracted from downloaded archive"""
    return os.path.join(DBS_DIR, "rgi", "data", "{db}", file_name).format(db=config["rgi"]["db"]["data"]["version"])

def rgi_get_extracted_variants_target(file_name):
    """For snakemake: get file path for an RGI variants data file extracted from downloaded archive"""
    return os.path.join(DBS_DIR, "rgi", "variants", "{db}", file_name).format(db=config["rgi"]["db"]["variants"]["version"])

def rgi_get_data_annotation_target(local=False):
    """For snakemake: Get file path for RGI data annotation file"""
    prefix = "" if local else os.path.join(DBS_DIR, "rgi")
    return os.path.join(prefix, "card_database_v{db}.fasta".format(db=config["rgi"]["db"]["data"]["version"]))

def rgi_get_variations_annotation_target(local=False):
    """For snakemake: Get file path for RGI variation data annotation file"""
    prefix = "" if local else os.path.join(DBS_DIR, "rgi")
    return os.path.join(prefix, "wildcard_database_v{db}.fasta".format(db=config["rgi"]["db"]["variants"]["version"]))

def get_rgi_bwt_output_names():
    """RGI bwt output file names (without their prefix)"""
    # Ref: https://github.com/arpcard/rgi#rgi-bwt-tab-delimited-output-details
    return ["allele_mapping_data.txt", "gene_mapping_data.txt", "artifacts_mapping_stats.txt", "overall_mapping_stats.txt", "reference_mapping_stats.txt"]