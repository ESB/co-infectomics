##################################################
# HUMAnN 3.0
# https://github.com/biobakery/humann
# 
# https://github.com/biobakery/humann#standard-workflow:
#   0. setup (databases)
#   1. profiling
#   2. normalization
#   3. join tables

# NOTE:
#   *.done has to be outside of the created sub-folders (i.e. chocophlan/ and uniref/)
#   DB paths set via humann_config need to include the sub-folders (i.e. chocophlan/ and uniref/)
rule humann3_db:
    output:
        os.path.join(DBS_DIR, "humann3", "{db}.done")
    log:
        os.path.join(DBS_DIR, "humann3", "{db}.log"),
    params:
        db_version=lambda wildcards: config["humann3"]["db"][wildcards.db],
        db_type=get_humann3_db_type,
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "HUMAnN 3.0: databases: {output}"
    shell:
        "(outf=$(realpath {output}) && "
        # download the database
        "humann_databases --download {wildcards.db} {params.db_version} $(dirname {output}) && "
        # update the location of the database
        "humann_config --update database_folders {params.db_type} ${{outf%.*}} "
        ") &> {log} && touch {output}"

# NOTE:
#   The MetaPhlAn 3.0 database files included in the conda env. are not complete/corrupted
#   resulting in an error ("Error reading _ebwt[] array: no more data")
rule humann3_db_metaphlan:
    output:
        os.path.join(DBS_DIR, "metaphlan3", "database.done"),
    log:
        os.path.join(DBS_DIR, "metaphlan3", "database.log"),
    params:
        db_version=lambda wildcards: config["humann3"]["db"]["metaphlan3"],
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "HUMAnN 3.0: MetaPhlAn 3.0 databases: {output}"
    shell:
        "metaphlan --install {params.db_version} --bowtie2db $(dirname {output}) &> {log} && touch {output}"

# NOTE:
#   FASTQ file should be decompressed
#   Since some temp files should be saved the flag "--remove-temp-output" is NOT used
rule humann3_profiling:
    input:
        # reads
        r1="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r1.preprocessed.fq" % RUNID,
        r2="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.r2.preprocessed.fq" % RUNID,
        se="preproc/{mtype}/{sid}/%s/Preprocessing/{mtype}.se.preprocessed.fq" % RUNID,
        # DBs
        dbs=expand(os.path.join(DBS_DIR, "humann3", "{db}.done"), db=["chocophlan", "uniref"]),
        mphl=os.path.join(DBS_DIR, "metaphlan3", "database.done"),
    output:
        fq=temp("humann3/{mtype}/{sid}.tmp.fq"),
        gf="profiling/humann3/{mtype}/{sid}_genefamilies.tsv",
        pc="profiling/humann3/{mtype}/{sid}_pathcoverage.tsv",
        pa="profiling/humann3/{mtype}/{sid}_pathabundance.tsv",
        mp="profiling/humann3/{mtype}/{sid}_metaphlan_bowtie2.txt.gz", # MetaPhlAn Bowtie2 output
        tf=temp(directory("profiling/humann3/{mtype}/{sid}_humann_temp/")), # temp folder
    log:
        tool="profiling/humann3/{mtype}/{sid}.log",
        smk="profiling/humann3/{mtype}/{sid}.smk.log",
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads: 10
    params:
        params=lambda wildcards: "--input-format fastq --verbose --output-basename {}".format(wildcards.sid),
        mp_db_version=lambda wildcards: config["humann3"]["db"]["metaphlan3"],
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "HUMAnN 3.0: {input.r1}, {input.r2}, {input.se}"
    shell:
        # input reads
        "(cat {input.r1} {input.r2} {input.se} > {output.fq} && "
        # profiling
        "humann -i {output.fq} -o $(dirname {output.gf}) {params.params} "
        "--metaphlan-options \"--index {params.mp_db_version} --bowtie2db $(dirname {input.mphl})\" "
        "--threads {threads} --o-log {log.tool}) &> {log.smk} && "
        # copy some temp files
        "cat {output.tf}/{wildcards.sid}_metaphlan_bowtie2.txt | gzip > {output.mp}"

rule humann3_rab:
    input:
        gf="profiling/humann3/{mtype}/{sid}_genefamilies.tsv",
        pa="profiling/humann3/{mtype}/{sid}_pathabundance.tsv",
    output:
        gf=temp("profiling/humann3/{mtype}/{sid}_genefamilies_rab.tsv"),
        pa=temp("profiling/humann3/{mtype}/{sid}_pathabundance_rab.tsv"),
    log:
        "profiling/humann3/{mtype}/{sid}.rab.log"
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "HUMAnN 3.0: relative abundance: {input}"
    shell:
        "humann_renorm_table --input {input.gf} --output {output.gf} --units relab &> {log} && "
        "humann_renorm_table --input {input.pa} --output {output.pa} --units relab &>> {log}"

rule humann3_join:
    input:
        lambda wildcards: expand(
            "profiling/humann3/{{mtype}}/{sid}_{fname}.tsv",
            sid=SIDS_MG if wildcards.mtype == "mg" else SIDS_MT,
            fname=["genefamilies_rab", "pathabundance_rab", "pathcoverage"],
        ),
    output:
        gf="profiling/humann3/{mtype}/joined_genefamilies_rab.tsv",
        pc="profiling/humann3/{mtype}/joined_pathcoverage.tsv",
        pa="profiling/humann3/{mtype}/joined_pathabundance_rab.tsv",
    log:
        "profiling/humann3/{mtype}/join.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
    threads: 10 # need mem    
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "HUMAnN 3.0: join tables: {wildcards.mtype}"
    shell:
        "humann_join_tables --input $(dirname {input[0]}) --output {output.gf} --file_name genefamilies_rab &> {log} &&"
        "humann_join_tables --input $(dirname {input[0]}) --output {output.pa} --file_name pathabundance_rab &>> {log} &&"
        "humann_join_tables --input $(dirname {input[0]}) --output {output.pc} --file_name pathcoverage &>> {log}"
