##################################################
# MetaPhlAn 3.0
# https://github.com/biobakery/MetaPhlAn
# https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0

# NOTE: database installation is in humann3.smk (rule humann3_db_metaphlan)

# https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0#basic-usage
# https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn2#full-command-line-options (NOTE: v2, not v3.0)
rule metaphlan3_profiling_rab:
    input:
        # mapping (done by humann3, see humann3.smk)
        bwt2o="profiling/humann3/{mtype}/{sid}_metaphlan_bowtie2.txt.gz",
        # DBs
        mp_db=os.path.join(DBS_DIR, "metaphlan3", "database.done"),
    output:
        bwt2o=temp("profiling/metaphlan3/{mtype}/{sid}.bowtie2"),
        tsv="profiling/metaphlan3/{mtype}/{sid}_rab.tsv",
    log:
        "profiling/metaphlan3/{mtype}/{sid}_rab.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads: 4
    params:
        # sample ID; input type; report tax. levels; allow the profiling of viruses; report relat. abund.
        params=lambda wildcards: "--sample_id_key {sid} --sample_id {sid} --input_type bowtie2out --tax_lev 'a' --add_viruses -t 'rel_ab'".format(sid=wildcards.sid),
        # DB version
        mp_db_version=lambda wildcards: config["humann3"]["db"]["metaphlan3"],
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "MetaPhlAn 3.0: {input.bwt2o}"
    shell:
        "zcat {input.bwt2o} > {output.bwt2o} && "
        "metaphlan {output.bwt2o} --index {params.mp_db_version} --bowtie2db $(dirname {input.mp_db}) --output_file {output.tsv} {params.params} --nproc {threads} &> {log}"

# https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0#merging-tables
rule metaphlan3_join:
    input:
        lambda wildcards: expand(
            "profiling/metaphlan3/{{mtype}}/{sid}_rab.tsv",
            sid=SIDS_MG if wildcards.mtype == "mg" else SIDS_MT,
        ),
    output:
        "profiling/metaphlan3/{mtype}/joined_rab.tsv"
    log:
        "profiling/metaphlan3/{mtype}/joined_rab.log"
    wildcard_constraints:
        mtype="|".join(MTYPES),
    threads: 10 # need mem    
    conda:
        os.path.join(ENV_DIR, "humann3.yaml")
    message:
        "MetaPhlAn 3.0: join tables: {wildcards.mtype}"
    shell:
        "merge_metaphlan_tables.py {input} > {output} 2> {log}"