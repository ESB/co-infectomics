##################################################
# The Resistance Gene Identifier (RGI)
# https://github.com/arpcard/rgi
# https://card.mcmaster.ca
#
# For the download and installation of databases see https://github.com/arpcard/rgi#rgi-databases
# including "Additional Reference Data for Metagenomics Analyses"
# The following script was also used as a reference: https://github.com/arpcard/rgi/blob/master/test.sh

rule rgi_download:
    output:
        # temp: downloaded archives
        arch_data=temp(rgi_get_data_download_target()),
        arch_vars=temp(rgi_get_variants_download_target()),
        # extracted files required for other rules
        # NOTE: the donwloads include also other files not listed here
        card=rgi_get_extracted_data_target("card.json"),
        widx=rgi_get_extracted_variants_target("index-for-model-sequences.txt"),
        kdb=rgi_get_extracted_variants_target("61_kmer_db.json"),
        kamr=rgi_get_extracted_variants_target("all_amr_61mers.txt"),
    log:
        os.path.join(DBS_DIR, "rgi", "download.log")
    params:
        url_data=config["rgi"]["db"]["data"]["url"],
        url_vars=config["rgi"]["db"]["variants"]["url"],
    message:
        "RGI: download data: {params.url_data}, {params.url_vars}"
    shell:
        # download
        "wget -O {output.arch_data} {params.url_data} 2> {log} && "
        "wget -O {output.arch_vars} {params.url_vars} 2>> {log} && "
        # decompress
        "tar -xvf {output.arch_data} -C $(dirname {output.card}) &> {log} && "
        "tar -xvf {output.arch_vars} -C $(dirname {output.widx}) &>> {log} && "
        "gunzip $(dirname {output.widx})/*.gz"

# Create other data required to process metagenomic data
# https://github.com/arpcard/rgi#rgi-databases, see "Additional Reference Data for Metagenomics Analyses"
# 1) CARD's Resistomes & Variants data
# 2) WildCARD data
rule rgi_annotations:
    input:
        card=rules.rgi_download.output.card,
        widx=rules.rgi_download.output.widx,
    output:
        # temp local files (because RGI creates them in working directory)
        cannot_temp=temp(rgi_get_data_annotation_target(local=True)),
        wannot_temp=temp(rgi_get_variations_annotation_target(local=True)),
        # copy of local files
        cannot=rgi_get_data_annotation_target(local=False),
        wannot=rgi_get_variations_annotation_target(local=False),
    log:
        os.path.join(DBS_DIR, "rgi", "annotations.log")
    params:
        vversion=config["rgi"]["db"]["variants"]["version"],
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "RGI: create annotations: {input}"
    shell:
        "(rgi card_annotation --input {input.card} && "
        "rgi wildcard_annotation --input_directory $(dirname {input.widx}) --card_json {input.card} -v \"{params.vversion}\" && "
        "rsync -tP {output.cannot_temp} {output.cannot} && "
        "rsync -tP {output.wannot_temp} {output.wannot}) &> {log}"

# Load created additional data required to process metagenomic data
# https://github.com/arpcard/rgi#rgi-databases, see "Additional Reference Data for Metagenomics Analyses"
# 1) CARD's Resistomes & Variants data
# 2) WildCARD data
# 3) CARD Reference Data and Additional Reference Data for Metagenomics Analyses
rule rgi_load:
    input:
        card=rules.rgi_download.output.card,
        cannot=rules.rgi_annotations.output.cannot,
        wannot=rules.rgi_annotations.output.wannot,
        widx=rules.rgi_download.output.widx,
        kdb=rules.rgi_download.output.kdb,
        kamr=rules.rgi_download.output.kamr,
    output:
        loaded=os.path.join(DBS_DIR, "rgi", "loaded")
    log:
        os.path.join(DBS_DIR, "rgi", "loaded.log")
    params:
        kmer=61,
        vversion=config["rgi"]["db"]["variants"]["version"],
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "RGI: load additional data: {input}"
    shell:
        "(rgi load --card_json {input.card} --card_annotation {input.cannot} "
        "--wildcard_index {input.widx} --wildcard_version \"{params.vversion}\" --wildcard_annotation {input.wannot} --debug --local && "
        # "(rgi load -i {input.card} --card_annotation {input.cannot} --local && "
        # "rgi load --wildcard_annotation {input.wannot} --wildcard_index {input.widx} --card_annotation {input.cannot} --local && "
        "rgi load --kmer_database {input.kdb} --amr_kmers {input.kamr} --kmer_size {params.kmer} --debug --local && "
        "rgi database --version --all --local && touch {output.loaded}) &> {log}"

# Run one job to create required index files for the chosen aligner
# NOTE: Needed since otherwise multiple jobs would try to create the index in parallel leading to errors.
rule rgi_profiling_init:
    input:
        se=os.path.join("preproc", "mg", SIDS_MG[0], RUNID, "Preprocessing", "mg.se.preprocessed.fq"),
        loaded=rules.rgi_load.output.loaded,
    output:
        init=os.path.join(DBS_DIR, "rgi", "init.done"),
        odir=temp(directory(os.path.join("rgi_testrun"))),
    log:
        os.path.join(DBS_DIR, "rgi", "init.log")
    threads: 5
    params:
        # use bowtie2 (no preference, incl. in examples), rm temp files, debug info, incl. wildCARD ref., local DB
        params="-a bowtie2 --clean --debug --include_wildcard --local",
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "RGI: profiling init: {input}"
    shell:
        "(mkdir -p {output.odir} && rgi database --version --all --local > {log} && "
        "rgi bwt -1 {input.se} --output_file {output.odir}/test {params.params} -n {threads}) &> {log} && "
        "touch {output.init}"

# Profiling
# https://github.com/arpcard/rgi#using-rgi-bwt-metagenomic-short-reads-genomic-short-reads
# NOTE: This is an unpublished algorithm undergoing beta-testing
# NOTE: No filters for mapq, mapped, and coverage (yet), but values are reported for manual filtering
rule rgi_profiling:
    input:
        # reads
        r1=os.path.join("preproc", "{mtype}", "{sid}", RUNID, "Preprocessing", "{mtype}.r1.preprocessed.fq"),
        r2=os.path.join("preproc", "{mtype}", "{sid}", RUNID, "Preprocessing", "{mtype}.r2.preprocessed.fq"),
        se=os.path.join("preproc", "{mtype}", "{sid}", RUNID, "Preprocessing", "{mtype}.se.preprocessed.fq"),
        # DBs
        loaded=rules.rgi_load.output.loaded,
        init=rules.rgi_profiling_init.output.init,
    output:
        pe=expand(
            os.path.join("profiling", "rgi", "{{mtype}}", "{{sid}}.pe.{suffix}"),
            suffix=get_rgi_bwt_output_names()
        ),
        se=expand(
            os.path.join("profiling", "rgi", "{{mtype}}", "{{sid}}.se.{suffix}"),
            suffix=get_rgi_bwt_output_names()
        ),
    log:
        os.path.join("profiling", "rgi", "{mtype}", "{sid}.log")
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    threads: 10
    params:
        # use bowtie2 (no preference, incl. in examples), rm temp files, debug info, incl. wildCARD ref., local DB
        params="-a bowtie2 --clean --debug --include_wildcard --local",
        # output prefix (same as the one used for the output)
        prefix_pe=lambda wildcards: os.path.join("profiling", "rgi", wildcards.mtype, "{sid}.pe".format(sid=wildcards.sid)),
        prefix_se=lambda wildcards: os.path.join("profiling", "rgi", wildcards.mtype, "{sid}.se".format(sid=wildcards.sid)),
    conda:
        os.path.join(ENV_DIR, "rgi.yaml")
    message:
        "RGI: profiling: {input.r1}, {input.r2}, {input.se}"
    shell:
        "rgi database --version --all --local > {log} && "
        "rgi bwt -1 {input.r1} -2 {input.r2} --output_file {params.prefix_pe} {params.params} -n {threads} 2>> {log} && "
        "rgi bwt -1 {input.se} --output_file {params.prefix_se} {params.params} -n {threads} 2>> {log}"

# Genes found in a sample (from filtered PE/SE hits)
rule rgi_found_genes:
    input:
        pe=os.path.join("profiling", "rgi", "{mtype}", "{sid}.pe.gene_mapping_data.txt"),
        se=os.path.join("profiling", "rgi", "{mtype}", "{sid}.se.gene_mapping_data.txt"),
    output:
        genes=temp(os.path.join("profiling", "rgi", "{mtype}", "{sid}.genes.txt"))
    wildcard_constraints:
        mtype="|".join(MTYPES),
        sid="|".join(SAMPLES.index),
    params:
        # cutoffs
        ave_perc_cov=50,
        ave_mapq=0,
    message:
        "RGI: profiling: genes found in filtered PE/SE hits: {input}"
    run:
        pe = read_rgi_profiling_gene_report(input.pe)
        se = read_rgi_profiling_gene_report(input.se)
        pe = pe.loc[(pe["Average Percent Coverage"] > params.ave_perc_cov) & (pe["Average MAPQ (Completely Mapped Reads)"] > params.ave_mapq), "ARO Accession"]
        se = se.loc[(se["Average Percent Coverage"] > params.ave_perc_cov) & (se["Average MAPQ (Completely Mapped Reads)"] > params.ave_mapq), "ARO Accession"]
        with open(output.genes, "w") as ofile:
            ofile.write("\n".join(set(pe).union(se)))

rule rgi_join_found_genes:
    input:
        lambda wildcards: expand(
            os.path.join("profiling", "rgi", "{{mtype}}", "{sid}.genes.txt"),
            sid=SIDS_MG if wildcards.mtype == "mg" else SIDS_MT,
        ),
    output:
        genes="profiling/rgi/{mtype}/joined_genes.tsv"
    wildcard_constraints:
        mtype="|".join(MTYPES), 
    message:
        "RGI: profiling: genes found in filtered PE/SE hits: all samples"
    run:
        def get_sid_from_file(fname):
            return re.sub("\.genes\.txt", "", os.path.basename(fname))
        
        def read_genes(fname):
            return pandas.read_csv(ifile, header=None, dtype='str').iloc[:,0]
        # AROs
        aros = set()
        for ifile in input:
            aros = aros.union(read_genes(ifile))
        # fill
        genes = pandas.DataFrame(0, index=aros, columns=[get_sid_from_file(ifile) for ifile in input])
        for ifile in input:
            genes.loc[read_genes(ifile),get_sid_from_file(ifile)] = 1 # present
        # save
        genes.to_csv(output.genes, sep="\t", header=True, index=True, index_label="ARO")